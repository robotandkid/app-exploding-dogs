import React, { ReactNode } from "react";
import styled from "styled-components";
import {
  AppColumnGrid,
  AppThemeProps,
} from "../src/components/ui/globalStyles";
import { appStore } from "../src/state/appState";
import {
  GameNotStarted,
  GameNotStartedAppState,
  GameStarted,
  GameWaitingRoomAppState,
  InitAppState,
  LobbyAppState,
  Player,
  PlayerCreateAppState,
  PlayerLoadAppState,
  PlayGameAppState,
  RegisterPlayerAppState,
} from "../src/state/appState.types";
import { AppWrapper } from "../src/components/AppWrapper";
import { modalId } from "../src/components/ui/Modal/Modal";
import { toastId } from "../src/components/ui/Toast/Toast";

export const mockPlayer = (id: string, name: string) =>
  ({
    id,
    name,
    active: true,
  } as Player);

export const mockInitAppState = (): InitAppState => ({
  player: undefined,
  game: undefined,
  status: "init",
});

export const mockRegisterPlayerAppState = (): RegisterPlayerAppState => ({
  player: undefined,
  game: undefined,
  status: "player-registration",
});

export const mockPlayerCreateAppState = (
  playerToLoad: string = "foobar"
): PlayerCreateAppState => ({
  player: undefined,
  game: undefined,
  status: "try-to-create-player",
  playerToLoad,
});

export const mockPlayerLoadAppState = (
  playerIdToLoad: string = "1"
): PlayerLoadAppState => ({
  player: undefined,
  game: undefined,
  status: "try-to-load-player",
  playerIdToLoad,
});

export const mockGameNotStartedAppState = (
  players: Player[]
): GameNotStartedAppState => ({
  player: players[0],
  game: {
    id: "promo",
    gameCode: "xjwe20",
    startedByPlayerId: players[0].id,
    players: players.reduce((total, cur) => ({ ...total, [cur.id]: cur }), {}),
    watchers: {},
  } as GameNotStarted,
  status: "game-not-started",
});

export const mockGameWaitingRoomAppState = (
  player: Player
): GameWaitingRoomAppState => ({
  player: player,
  game: undefined,
  status: "game-waiting-room",
});

export const mockGameLobbyState = (player: Player): LobbyAppState => ({
  player: player,
  game: undefined,
  status: "lobby",
});

export const mockPlayGameState = (props: {
  players: Player[];
  currentPlayerId: string;
  direction: GameStarted["direction"];
}): PlayGameAppState => ({
  player: props.players[0],
  game: {
    id: "promo",
    gameCode: "xjwe20",
    startedByPlayerId: props.players[0].id,
    players: props.players.reduce(
      (total, cur) => ({ ...total, [cur.id]: cur }),
      {}
    ),
    playerOrderById: props.players.map(({ id }) => id),
    watchers: {},
    started: true,
    done: false,
    currentPlayerId: props.currentPlayerId,
    direction: props.direction,
  },
  status: "play-game",
});

export const Page = styled(AppColumnGrid)`
  ${(props: AppThemeProps) => props.theme.pageWidth};
  height: 100%;
  border: 1px solid black;
`;

export interface Fixture {
  store: typeof appStore;
  name: string;
}

export function toFixture(fixtures: Fixture[], component: ReactNode) {
  return fixtures.reduce(
    (total, cur) => ({
      ...total,
      [cur.name]: (
        <>
          <AppWrapper store={cur.store}>{component}</AppWrapper>
          <div id={modalId}></div>
          <div id={toastId}></div>
        </>
      ),
    }),
    {} as { [name: string]: ReactNode }
  );
}
