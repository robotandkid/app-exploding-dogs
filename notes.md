## June 1, 2020

- Originally tried xstate. This was very promising---can do nested, parallel
  state machines, guards. Main problems with xstate:
  - performance unclear
  - poor typescript support. For example, can dispatch events that don't exist
    if I'm not mistaken.
  - too much boilerplate. Resulting xstate schema is difficult to understand.
    Reminds me of XML
- On the other hand, redux as a state machine is messy:
  - State transition logic is not colocated. Thunks/sagas can compete with
    reducers to handle state transitions.
  - This can make unit tests unreliable---unit tests can pass but integration
    tests fail.
  - State can be typed but not state transitions.
- So this repo uses redux with a simple, typed state machine. No parallel or
  nested machines but state transitions are typed, that is, you cannot go to an
  unsupported state because the compiler will complain.

```typescript
//...
goToPlayerLoad: stateTransition(
  "init",
  "try-to-load-player",
  (state, action: PayloadAction<{ playerIdToLoad: string }>) => ({
    ...state,
    currentPlayer: undefined,
    game: undefined,
    status: "try-to-load-player",
    playerIdToLoad: action.payload.playerIdToLoad,
  })
);
```

```typescript
const appStatus: AppStatus = useSelector<AppState, AppStatus>(getAppStatus);

if (appStatus === "init") {
  dispatch(
    appSlice.actions.goToPlayerLoad({
      // won't compile unless dispatched from init state
      prevStatus: appStatus,
      playerIdToLoad,
    })
  );
}
```

## July 8, 2020

- While typescript is very promising (type states _and_ transitions), the
  tooling may not be there yet....
- `createSlice` is a powerful way to create actions/reducers with minimal
  boilerplate but...
- as far as I can tell, the intent is for each slice to be independent reducers.
  For example, checkout the `modalSlice`.
- This makes it difficult to put app state into separate slices w/o losing
  compile-time transition checks.
- The reason is because the compile-time checks check the _entire_ app state.
  This is difficult (but not impossible) when the app state is split across
  slices.
- Definitely worth exploring in the future but for now leaving most of the app
  in a giant slice.
