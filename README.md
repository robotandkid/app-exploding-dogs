# Exploding Puppies

## Screenshots

![image.png](./image.png)

## Tech Stack

- netlify - hosting
- nextjs - statically generated site
- firebase - cloud storage
- typescript
- react
- redux
- styled-components
- pixi.js
- jest

## Setup

### Setup Firebase

- Add project
- Add app to project
- Add a cloud firestore to app
- Copy firebaseConfig into .env. (See `.env.sample`.)

### Setup Netlify

- add env variables

## Development

```
yarn dev
```

### Component Explorer (via Cosmos)

```
yarn cosmos
```

### Testing

- testing is done thru Jest.
- packages installed for testing:
  - babel-jest
  - @babel/core
  - @babel/preset-env
  - @babel/preset-typescript

## Deploy Locally

```
yarn export
npx http-serve ./out
```
