# TODOs

## Techdebt

- [x] TODO animations, the styled-components way
- [x] Create separate slices for each different domain---modals, playersData
      Not possible w/o significant refactoring.

## Features

- [ ] TODO firestore user presence detection i.e., show "left"
- [ ] TODO joining game toast
- [ ] TODO handle all AJAX errors
- [ ] TODO video chat
- [ ] TODO p2p connections once game starts

### a11y

- [ ] TODO accessible toast
- [ ] TODO a11y animations: prefers-reduced-motion
- [ ] https://www.w3.org/WAI/tutorials/ - https://www.w3.org/WAI/people-use-web/ - https://www.w3.org/WAI/fundamentals/accessibility-principles/ - https://www.w3.org/WAI/WCAG21/quickref/

## Bugs

- [ ] TODO waiting room - disable buttons on click
- [ ] TODO lobby - disable buttons on click
- [ ] TODO buttons now have an extra border (incorrect)
- [x] TODO clear out game code in dialog on close
- [x] TODO remove hyphen from game code in dialog
- [x] TODO when player name is really long, unexpected game code line break in
      waitingRoom
- [x] TODO really long names don't render well on lobby

## Components

### Players

- [x] TODO When new player added, join game button should be enabled
- [x] TODO fix layout in players
- [ ] TODO add player animations

## Mobile

- [x] TODO auto-show keyboard on input focus. Not possible unless the user first
      clicks something.
      https://stackoverflow.com/questions/6837543/show-virtual-keyboard-on-mobile-phones-in-javascript
- [x] TODO full page dialog modals
- [x] TODO vertical layout for waiting room
- [x] TODO force landscape orientation
      https://stackoverflow.com/questions/14360581/force-landscape-orientation-mode
      Doesn't seem to work in Chrome/Brave
