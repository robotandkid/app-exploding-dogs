import firebase from "firebase/app";
import { maxNumberOfPlayers } from "../state/appState.types";
import { sixHoursAgo } from "../utils/time";
import { gameId } from "./firebase";
export interface FirebasePlayer {
  name: string;
  id: string;
  active: boolean;
  created: firebase.firestore.FieldValue;
}
interface FirebaseBaseGameState {
  id: string;
  created: firebase.firestore.Timestamp;
  startedByPlayerId: string;
  gameCode: string;
  players: { [id: string]: boolean };
  playerOrderById: string[];
  watchers: { [id: string]: boolean };
  started: boolean;
  done: boolean;
}

export interface FirebaseGameNotStarted extends FirebaseBaseGameState {
  started: false;
  done: false;
}

export interface FirebaseGameStarted extends FirebaseBaseGameState {
  started: true;
  done: false;
}

export interface FirebaseGameOver extends FirebaseBaseGameState {
  started: true;
  done: true;
}
export type FirebaseGameState =
  | FirebaseGameNotStarted
  | FirebaseGameStarted
  | FirebaseGameOver;

export function isGameExpired(game: FirebaseGameState) {
  return game.created.toMillis() <= sixHoursAgo().getTime();
}

export const canJoinGameAsPlayer = (player: Pick<FirebasePlayer, "id">) => (
  game: FirebaseGameState
): game is FirebaseGameNotStarted => {
  const alreadyJoined = !!game.players[player.id];
  const playerLimitNotReached =
    Object.keys(game.players).length < maxNumberOfPlayers;

  return (
    !game.done && (alreadyJoined || (playerLimitNotReached && !game.started))
  );
};

export const canJoinGameAsWatcher = (player: Pick<FirebasePlayer, "id">) => (
  game: FirebaseGameState
): game is FirebaseGameStarted => {
  const isAplayer = !!game.players[player.id];
  const alreadyJoined = !!game.watchers[player.id];

  return !game.done && !isAplayer && (alreadyJoined || game.started);
};

export function createFirebaseGameState(props: {
  gameCode: string;
  player: Pick<FirebasePlayer, "id">;
}): FirebaseGameNotStarted {
  return {
    id: gameId(props.gameCode),
    gameCode: props.gameCode,
    created: firebase.firestore.Timestamp.fromDate(new Date()),
    startedByPlayerId: props.player.id,
    players: {
      [props.player.id]: true,
    },
    playerOrderById: [props.player.id],
    watchers: {},
    started: false,
    done: false,
  };
}

export function addPlayerToGame(
  player: Pick<FirebasePlayer, "id">,
  game: FirebaseGameState
): FirebaseGameState {
  return {
    ...game,
    players: {
      ...game.players,
      [player.id]: true,
    },
    playerOrderById: [...game.playerOrderById, player.id],
  };
}

export function addWatcherToGame(
  player: Pick<FirebasePlayer, "id">,
  game: FirebaseGameState
): FirebaseGameState {
  return {
    ...game,
    watchers: {
      ...game.watchers,
      [player.id]: true,
    },
  };
}
