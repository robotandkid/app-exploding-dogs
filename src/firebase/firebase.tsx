import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import {
  Player,
  GameState,
  GameNotStarted,
  PartialGameState,
} from "../state/appState.types";
import { gameCode as _gameCode } from "../utils/gameCode";
import {
  FirebasePlayer,
  FirebaseGameState,
  isGameExpired,
  FirebaseGameNotStarted,
  createFirebaseGameState,
  canJoinGameAsPlayer,
  addPlayerToGame,
  canJoinGameAsWatcher,
  addWatcherToGame,
} from "./firebase.types";

const firebaseConfig = {
  apiKey: process.env.FIREBASE_API_KEY,
  authDomain: process.env.FIREBASE_AUTH_DOMAIN,
  databaseURL: process.env.FIREBASE_DATABASE_URL,
  projectId: process.env.FIREBASE_PROJECT_ID,
  storageBucket: process.env.FIREBASE_STORAGE_BUCKET,
  messagingSenderId: process.env.FIREBASE_MESSAGING_SENDER_ID,
  appId: process.env.FIREBASE_APP_ID,
  measurementId: process.env.FIREBASE_MEASUREMENT_ID,
};

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

const db = firebase.firestore();
const playersRef = db.collection("players");
const gamesRef = db.collection("games");

export function addPlayer(name: string): Promise<Player> {
  const created = firebase.firestore.FieldValue.serverTimestamp();

  return playersRef
    .add({
      name,
      active: true,
      created,
    } as Partial<FirebasePlayer>)
    .then(function (docRef) {
      return toGamePlayer({
        name,
        active: true,
        id: docRef.id,
        created,
      });
    });
}

export function getPlayer(id: string): Promise<Player | undefined> {
  return playersRef
    .where(firebase.firestore.FieldPath.documentId(), "==", id)
    .limit(1)
    .get()
    .then((querySnapshot) => {
      if (querySnapshot.empty) {
        return undefined;
      }

      const player = {
        ...querySnapshot.docs[0].data(),
        id,
      } as FirebasePlayer;

      return toGamePlayer(player);
    });
}

export const gameId = (gameCode: string) => `code:${gameCode}`;

function toGameState<T extends GameState>(
  game: FirebaseGameState,
  players: Array<FirebasePlayer | Player> = []
) {
  return {
    id: game.id,
    startedByPlayerId: game.startedByPlayerId,
    gameCode: game.gameCode,
    players: players.reduce(
      (total, cur) => ({
        ...total,
        [cur.id]: toGamePlayer(cur as FirebasePlayer),
      }),
      {}
    ),
    playerOrderById: game.playerOrderById,
    watchers: {},
    started: game.started,
    done: game.done,
  } as T;
}

const toGamePlayer = (player: FirebasePlayer): Player => ({
  id: player.id,
  name: player.name,
  active: player.active,
});

export async function createGame(player: Player): Promise<GameNotStarted> {
  return await db.runTransaction(async (transaction) => {
    let gameCode: string;
    let gameRef: firebase.firestore.DocumentReference<firebase.firestore.DocumentData>;
    let gameDoc: firebase.firestore.DocumentSnapshot<firebase.firestore.DocumentData>;

    while (true) {
      gameCode = _gameCode();
      gameRef = gamesRef.doc(gameId(gameCode));
      gameDoc = await transaction.get(gameRef);

      if (!gameDoc.exists) {
        break;
      }

      const game = gameDoc.data() as FirebaseGameState;

      if (!isGameExpired(game!)) {
        break;
      }
    }

    const newGame: FirebaseGameNotStarted = createFirebaseGameState({
      gameCode,
      player,
    });

    if (gameDoc.exists) {
      transaction.update(gameRef, newGame);
    } else {
      transaction.set(gameRef, newGame);
    }

    return toGameState<GameNotStarted>(newGame, [player]);
  });
}

/**
 * Tries to join the game unless the game doesn't exist or is expired,
 * in which case it will create a brand new game.
 *
 * @param gameCode
 * @param player
 */
export async function tryToJoinGame(
  gameCode: string,
  player: Player
): Promise<GameState> {
  const result = await db.runTransaction(async (transaction) => {
    const gameRef = gamesRef.doc(gameId(gameCode));
    const gameDoc = await transaction.get(gameRef);

    if (!gameDoc.exists) {
      return undefined;
    }

    const game = gameDoc.data() as FirebaseGameState;
    const alreadyJoined =
      !!game.players[player.id] || !!game.watchers[player.id];

    if (isGameExpired(game)) {
      return undefined;
    }

    if (alreadyJoined) {
      return game;
    }

    if (canJoinGameAsPlayer(player)(game)) {
      const newGame = addPlayerToGame(player, game);

      transaction.update(gameRef, newGame);

      return newGame;
    }

    if (canJoinGameAsWatcher(player)(game)) {
      const newGame = addWatcherToGame(player, game);

      transaction.update(gameRef, newGame);

      return newGame;
    }

    return undefined;
  });

  if (result) {
    const players = await loadPlayers(Object.keys(result.players));

    return toGameState(result, players);
  }

  return await createGame(player);
}

export function loadPlayers(playerIds: string[]) {
  return playersRef
    .where(firebase.firestore.FieldPath.documentId(), "in", playerIds)
    .get()
    .then((docs) => {
      return docs.docs
        .map((doc) => ({ ...doc.data(), id: doc.id } as FirebasePlayer))
        .map(toGamePlayer);
    });
}

export type GameStateCallback = (
  game: PartialGameState,
  playerIds: string[]
) => void;

export function listenToGameState(
  gameCode: string,
  player: Player,
  cb: GameStateCallback
) {
  return gamesRef.doc(gameId(gameCode)).onSnapshot((querySnapshop) => {
    let game: FirebaseGameState;

    if (!querySnapshop.exists) {
      // TODO instead of creating a game, throw an error
      game = createFirebaseGameState({ gameCode, player });
    } else {
      game = querySnapshop.data() as FirebaseGameState;
    }

    cb(toGameState(game), Object.keys(game.players));
  });
}
