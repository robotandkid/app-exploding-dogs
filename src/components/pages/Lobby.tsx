import React, { useCallback, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import {
  appSlice,
  getAppStatus,
  getOpenModals,
  getPlayer,
  RootState,
} from "../../state/appState";
import { AppStatus, Player } from "../../state/appState.types";
import { useAppRouter } from "../../state/route";
import { formattedGameCode, gameCodeQueryParam } from "../../utils/gameCode";
import { Avatar } from "../Avatar/Avatar";
import { PrimaryButton, SecondaryButton } from "../ui/Button/Button";
import {
  AppColumnGrid,
  AppRowGrid,
  AppThemeProps,
  breakWord,
  Input,
  pageTransitionFlyIn,
  Text1,
  Text2,
  Text3,
} from "../ui/globalStyles";
import { Modal } from "../ui/Modal/Modal";
import { useTakeUntilTrue } from "../../utils/bacon";

const lobbyLabels = {
  welcome: "Welcome!",
  createGameButton: "Create a Game",
  joinGameButton: "Join a Game",
};

const joinGameModalLabels = {
  header: "Enter code",
  cancel: "Cancel",
  join: "Join",
};

const JoinGameAppColumnGrid = styled(AppColumnGrid)`
  justify-content: space-between;
  height: 100%;
`;

const JoinGameHeader = styled(Text3)`
  text-transform: uppercase;
  text-align: center;
`;

const JoinGameInput = styled(Input)`
  font-size: 2.5rem;
  width: 20rem;

  @media only screen and (min-width: 768px) {
    font-size: 3.25rem;
    width: 25rem;
  }
  @media only screen and (min-width: 300px) {
    font-size: 3rem;
    width: 15rem;
  }

  font-weight: 600;
  line-height: 1.2;
  margin: 0;
  padding: 0;
  margin-top: 1rem;
  margin-bottom: 1rem;
  padding: 0.75rem;
  border: 2px solid ${(props: AppThemeProps) => props.theme.colors.gray1};
  text-align: center;
`;

const StyledJoinGameButton = styled(PrimaryButton)`
  margin-left: 2rem;
`;

const joinGameModalDims = { width: "40rem", height: "17rem" };

function JoinGameModal(props: { onClose: () => void }) {
  const { onClose } = props;
  const { pathname, query, replace } = useAppRouter();
  const [code, setCode] = useState("");

  const onCodeChange = useCallback((possibleCode: string) => {
    setCode(formattedGameCode(possibleCode));
  }, []);

  const disableJoinButton = code.length <= 6;

  const joinGame = useCallback(() => {
    if (!disableJoinButton) {
      replace(
        pathname,
        { ...query, code: gameCodeQueryParam(code) },
        { shallow: true }
      );
      onClose();
    }
  }, [code, disableJoinButton, onClose, pathname, query, replace]);

  return (
    <Modal dims={joinGameModalDims} onClose={onClose}>
      <JoinGameAppColumnGrid>
        <JoinGameHeader>{joinGameModalLabels.header}</JoinGameHeader>
        <JoinGameInput
          type="text"
          value={code}
          onSubmit={joinGame}
          onChange={onCodeChange}
          focus
        />
        <AppRowGrid>
          <SecondaryButton onClick={onClose}>
            {joinGameModalLabels.cancel}
          </SecondaryButton>
          <StyledJoinGameButton onClick={joinGame} disabled={disableJoinButton}>
            {joinGameModalLabels.join}
          </StyledJoinGameButton>
        </AppRowGrid>
      </JoinGameAppColumnGrid>
    </Modal>
  );
}

const JoinGameModalOpenButton = styled(PrimaryButton)`
  margin-top: 2rem;
`;

function JoinGameButton() {
  const openModalCount = useSelector<RootState, number>(getOpenModals);
  const [showModal, setShowModal] = useState(false);

  const closeModal = useCallback(() => {
    setShowModal(false);
  }, []);

  return (
    <>
      <JoinGameModalOpenButton
        onClick={() => setShowModal(true)}
        disabled={openModalCount > 0}
      >
        {lobbyLabels.joinGameButton}
      </JoinGameModalOpenButton>
      {showModal && <JoinGameModal onClose={closeModal} />}
    </>
  );
}

function CreateGameButton() {
  const dispatch = useDispatch();
  const openModalCount = useSelector<RootState, number>(getOpenModals);
  const appStatus = useSelector<RootState, AppStatus>(getAppStatus);

  const showLobby = useTakeUntilTrue(appStatus === "lobby");

  if (!showLobby) {
    return null;
  }

  return (
    <PrimaryButton
      onClick={() =>
        appStatus === "lobby" &&
        dispatch(
          appSlice.actions.goToGameWaitingRoom({
            prevStatus: appStatus,
          })
        )
      }
      disabled={openModalCount > 0}
    >
      {lobbyLabels.createGameButton}
    </PrimaryButton>
  );
}

const Container = styled.article`
  display: flex;
  justify-content: center;
  padding: 2rem;

  @media only screen and (min-width: 768px) {
    padding: 0;
  }

  animation: ${pageTransitionFlyIn} 1.2s ease forwards;
`;

const ColumnGrid = styled(AppColumnGrid)`
  ${(props: AppThemeProps) => props.theme.pageWidth};
`;

const Welcome = styled(Text1)``;

const NameGrid = styled(AppRowGrid)`
  align-items: flex-start;
`;

const Name = styled(Text2)`
  text-align: center;
  max-width: 60vw;
  ${breakWord}
`;

const MainContentRowGrid = styled(AppRowGrid)`
  margin-top: 2rem;
  align-items: flex-start;
`;

const ButtonColumnGrid = styled(AppColumnGrid)`
  align-items: flex-start;
  margin-right: 2rem;
`;

const LobbyAvatar = styled(Avatar)`
  border: 5px solid black;
`;

export function Lobby() {
  const player = useSelector<RootState, Player | undefined>(getPlayer)!;
  const appStatus = useSelector<RootState, AppStatus>(getAppStatus);

  const showLobby = useTakeUntilTrue(appStatus === "lobby");

  if (!showLobby) {
    return null;
  }

  return (
    <Container>
      <ColumnGrid>
        <Welcome>{lobbyLabels.welcome}</Welcome>
        <NameGrid>
          <Name>&#9733;</Name>
          <Name>{player.name}</Name>
          <Name>&#9733;</Name>
        </NameGrid>
        <MainContentRowGrid>
          <ButtonColumnGrid>
            <CreateGameButton />
            <JoinGameButton />
          </ButtonColumnGrid>
          <LobbyAvatar player={player} />
        </MainContentRowGrid>
      </ColumnGrid>
    </Container>
  );
}
