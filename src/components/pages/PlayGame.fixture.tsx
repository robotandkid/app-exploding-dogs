import React from "react";
import {
  Fixture,
  mockInitAppState,
  toFixture,
} from "../../../cosmos/cosmos.fixtures";
import { AppStore } from "../../state/appState";
import { PlayGame } from "./PlayGame";

const fixtures: Fixture[] = [
  {
    store: AppStore({
      app: mockInitAppState(),
    }),
    name: "PlayGame",
  },
];

export default toFixture(fixtures, <PlayGame />);
