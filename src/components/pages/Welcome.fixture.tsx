import React from "react";
import {
  Fixture,
  mockInitAppState,
  mockPlayerCreateAppState,
  mockPlayerLoadAppState,
  mockRegisterPlayerAppState,
  toFixture,
} from "../../../cosmos/cosmos.fixtures";
import { AppStore } from "../../state/appState";
import { Welcome } from "./Welcome";

const fixtures: Fixture[] = [
  {
    store: AppStore({
      app: mockInitAppState(),
    }),
    name: "splash",
  },
  {
    store: AppStore({
      app: mockRegisterPlayerAppState(),
    }),
    name: "player registration",
  },
  {
    store: AppStore({
      app: mockPlayerCreateAppState(),
    }),
    name: "waiting on player creation",
  },
  {
    store: AppStore({
      app: mockPlayerLoadAppState(),
    }),
    name: "loading player",
  },
];

export default toFixture(fixtures, <Welcome />);
