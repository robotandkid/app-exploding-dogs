import React, { useCallback, useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import styled, { css } from "styled-components";
import { appSlice, getAppStatus, RootState } from "../../state/appState";
import { AppStatus } from "../../state/appState.types";
import { useTakeUntilTrue } from "../../utils/bacon";
import { AppThemeProps, breakWord } from "../ui/globalStyles";
import { TypewriterText } from "../ui/TypewriterText/TypewriterText";

const titleText = "Welcome to Exploding Puppies!";
const registrationText = "What’s your name?";

const loadingText = Array.from({ length: 1000 })
  .map(() => ".")
  .join("");

const textScrollingDurationMs = 50;
const loadingDurationMs = 200;

const Section = styled.section`
  display: flex;
  justify-content: center;
`;

const WelcomeContainer = styled.div`
  ${(props: AppThemeProps) => props.theme.pageWidth};
  ${(props: AppThemeProps) => props.theme.fontMainCss}

  padding : 2rem;

  @media only screen and (min-width: 768px) {
    padding: 0;
  }
`;

const TitleText = styled(TypewriterText)`
  ${(props: AppThemeProps) => props.theme.fontMainCss}

  font-size: 3.25rem;
  @media only screen and (min-width: 768px) {
    font-size: 4rem;
  }
  @media only screen and (min-width: 300px) {
    font-size: 3rem;
  }
  font-weight: 1000;
  line-height: 1.2;
  display: block;
`;

const text = css`
  ${(props: AppThemeProps) => props.theme.fontMainCss};

  font-size: 3rem;
  @media only screen and (min-width: 768px) {
    font-size: 3.75rem;
  }
  @media only screen and (min-width: 300px) {
    font-size: 2.6rem;
  }
  font-weight: 400;
  letter-spacing: -0.055rem;
`;

const Loading = styled(TypewriterText)`
  ${text}
  ${breakWord}
  display: block;
  margin-top: 1rem;
  line-height: 1;
`;

const RegistrationText = styled(TypewriterText)`
  ${text}
  margin-top: 1rem;
  display: block;
  ${breakWord}
`;

const Input = styled.div`
  ${text}
  color: black;
  margin-top: 1rem;
  outline: 0px solid transparent;
  ${breakWord}
`;

export const Welcome = () => {
  const dispatch = useDispatch();
  const appStatus = useSelector<RootState, AppStatus>(getAppStatus);
  const [splashDone, setSplashDone] = useState(false);

  const inputRef = useRef<HTMLDivElement>(null);

  const onSubmit = useCallback(
    (e: React.KeyboardEvent<HTMLDivElement>) => {
      const name = inputRef.current!.innerText;

      if (e.key === "Enter" && appStatus === "player-registration") {
        if (name && name.trim() !== "") {
          dispatch(
            appSlice.actions.tryToCreatePlayer({
              prevStatus: appStatus,
              playerToLoad: name,
            })
          );
        }
        e.preventDefault();
      }
    },
    [appStatus, dispatch]
  );

  const focusNameInput = useCallback(() => inputRef.current?.focus(), []);

  useEffect(() => {
    if (splashDone && appStatus === "player-loaded") {
      setTimeout(
        () =>
          dispatch(
            appSlice.actions.goToLobbyFromPlayerLoaded({
              prevStatus: appStatus,
            })
          ),
        2000
      );
    }
  }, [appStatus, dispatch, splashDone]);

  // Keep showing the registration once we transition to that state
  const showPlayerRegistration = useTakeUntilTrue(
    splashDone && appStatus === "player-registration"
  );
  // Keep showing player-create spinner once we transition to that state
  const showPlayerCreateSpinner = useTakeUntilTrue(
    splashDone && appStatus === "try-to-create-player"
  );
  // Keep showing player-load spinner once we transition to that state...
  // unless we need to show the player registration.
  // This case occurs when we couldn't load the player
  // So once we go to the player registration,
  // never show this spinner again
  const showPlayerLoadSpinner =
    useTakeUntilTrue(splashDone && appStatus === "try-to-load-player") &&
    !showPlayerRegistration;

  return (
    <Section>
      <WelcomeContainer>
        <TitleText
          typingDelayMs={textScrollingDurationMs}
          onDone={setSplashDone}
        >
          {titleText}
        </TitleText>
        {showPlayerLoadSpinner && (
          <Loading typingDelayMs={loadingDurationMs}>{loadingText}</Loading>
        )}
        {showPlayerRegistration && (
          <>
            <RegistrationText
              startTyping={splashDone}
              typingDelayMs={textScrollingDurationMs}
              onDone={focusNameInput}
            >
              {registrationText}
            </RegistrationText>
            <Input ref={inputRef} onKeyDown={onSubmit} contentEditable></Input>
          </>
        )}
        {showPlayerCreateSpinner && (
          <Loading typingDelayMs={loadingDurationMs}>{loadingText}</Loading>
        )}
      </WelcomeContainer>
    </Section>
  );
};
