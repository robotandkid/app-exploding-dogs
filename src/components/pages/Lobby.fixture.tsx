import React from "react";
import {
  Fixture,
  mockGameLobbyState,
  mockPlayer,
  toFixture,
} from "../../../cosmos/cosmos.fixtures";
import { AppStore } from "../../state/appState";
import { Lobby } from "./Lobby";

const fixtures: Fixture[] = [
  {
    store: AppStore({ app: mockGameLobbyState(mockPlayer("1", "muhahaha")) }),
    name: "Lobby",
  },
];

export default toFixture(fixtures, <Lobby />);
