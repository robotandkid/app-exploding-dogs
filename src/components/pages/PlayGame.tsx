import React, { useRef, useEffect, useCallback } from "react";
import styled from "styled-components";
import {
  AppThemeProps,
  pageTransitionFlyIn,
  AppColumnGrid,
} from "../ui/globalStyles";
import { usePixi } from "../ui/pixi/pixi";
import { avatarUrl } from "../Avatar/Avatar";
import { Texture, Sprite } from "pixi.js";

const Container = styled.article`
  display: flex;
  justify-content: center;
  padding: 2rem;

  @media only screen and (min-width: 768px) {
    padding: 0;
  }

  animation: ${pageTransitionFlyIn} 1.2s ease forwards;
`;

const ColumnGrid = styled(AppColumnGrid)`
  ${(props: AppThemeProps) => props.theme.pageWidth};
`;

export function PlayGame() {
  const { pixiRef } = usePixi({
    width: 100,
    height: 100,
    backgroundColor: 0x000000,
    init: useCallback((app, container) => {
      const texture = Texture.from(avatarUrl("foobar"));

      for (let i = 0; i < 25; i++) {
        const bunny = new Sprite(texture);
        bunny.anchor.set(0.5);
        bunny.x = (i % 5) * 40;
        bunny.y = Math.floor(i / 5) * 40;
        container.addChild(bunny);
      }

      container.x = app.screen.width / 2;
      container.y = app.screen.height / 2;

      container.pivot.x = container.width / 2;
      container.pivot.y = container.height / 2;
    }, []),
  });

  console.log("foobar");

  return (
    <Container>
      <ColumnGrid>
        <div ref={pixiRef}></div>
      </ColumnGrid>
    </Container>
  );
}
