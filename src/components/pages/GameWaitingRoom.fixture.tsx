import React from "react";
import {
  Fixture,
  mockGameNotStartedAppState,
  mockGameWaitingRoomAppState,
  mockPlayer,
  toFixture,
} from "../../../cosmos/cosmos.fixtures";
import { AppStore } from "../../state/appState";
import { GameWaitingRoom } from "./GameWaitingRoom";

const fixtures: Fixture[] = [
  {
    store: AppStore({
      app: mockGameWaitingRoomAppState(mockPlayer("1", "foobar")),
    }),
    name: "game loading",
  },
  {
    store: AppStore({
      app: mockGameNotStartedAppState([mockPlayer("1", "foobar")]),
    }),
    name: "waiting room",
  },
  {
    store: AppStore({
      app: mockGameNotStartedAppState([
        mockPlayer("1", "foobar"),
        mockPlayer("2", "anotherplayer"),
        mockPlayer("3", "yet another player"),
      ]),
    }),
    name: "can start game",
  },
];

export default toFixture(fixtures, <GameWaitingRoom />);
