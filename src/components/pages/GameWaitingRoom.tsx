import React, { useCallback, useState } from "react";
import { useSelector } from "react-redux";
import styled from "styled-components";
import {
  getAppStatus,
  getGameCode,
  getGamePlayersMap,
  RootState,
} from "../../state/appState";
import {
  AppStatus,
  gameWaitingRoomStatus,
  Player,
} from "../../state/appState.types";
import { GameWaitingRoomPlayers } from "../Players/GameWaitingRoomPlayers";
import { PrimaryButton } from "../ui/Button/Button";
import {
  AppColumnGrid,
  AppThemeProps,
  breakWord,
  pageTransitionFlyIn,
  Text4,
} from "../ui/globalStyles";
import { Toast } from "../ui/Toast/Toast";
import { TypewriterText } from "../ui/TypewriterText/TypewriterText";
import { formattedGameCode } from "../../utils/gameCode";

const creatingText = "Creating game";
const loadingText = Array.from({ length: 1000 })
  .map(() => ".")
  .join("");
const gameCodeCopyText = "Copy code";
const gameCodeCopiedText = "Code copied!";
const startGameButtonText = "Start game";

const appUrl = process.env.APP_URL;

function CopyCodeButton() {
  const gameCode = useSelector<RootState, string | undefined>(getGameCode);

  const [showToast, setShowToast] = useState(false);

  const copyGameCode = useCallback(() => {
    if (gameCode) {
      const url = `${appUrl}/?code=${gameCode}`;

      navigator.clipboard.writeText(url);

      setShowToast(true);
    }
  }, [gameCode]);

  return (
    <>
      <PrimaryButton onClick={copyGameCode}>{gameCodeCopyText}</PrimaryButton>
      <Toast show={showToast} onHide={() => setShowToast(false)}>
        {gameCodeCopiedText}
      </Toast>
    </>
  );
}

const StyledGameCode = styled(Text4)`
  margin-top: 1.5rem;
  margin-bottom: 3rem;
`;

export function GameCode() {
  const gameCode = useSelector<RootState, string | undefined>(getGameCode);

  return <StyledGameCode>{formattedGameCode(gameCode)}</StyledGameCode>;
}

const StyledStartGameButton = styled(PrimaryButton)`
  margin-bottom: 3rem;
`;

export function StartGameButton() {
  const playersMap = useSelector<
    RootState,
    { [id: string]: Player } | undefined
  >(getGamePlayersMap);
  const players = Object.keys(playersMap || {});
  const disabled = players.length <= 1;

  return (
    <StyledStartGameButton disabled={disabled}>
      {startGameButtonText}
    </StyledStartGameButton>
  );
}

const Container = styled.article`
  display: flex;
  justify-content: center;
  animation: ${pageTransitionFlyIn} 1.2s ease forwards;
`;

const ColumnGrid = styled(AppColumnGrid)`
  ${(props: AppThemeProps) => props.theme.pageWidth};
  height: 100%;
`;

const Loading = styled.div`
  ${(props: AppThemeProps) => props.theme.fontMainCss}
  font-size: 1.5rem;
  @media only screen and (min-width: 768px) {
    font-size: 1.25rem;
  }
  ${breakWord}
`;

export function GameWaitingRoom() {
  const appStatus = useSelector<RootState, AppStatus>(getAppStatus);

  if (!gameWaitingRoomStatus.includes(appStatus as any)) {
    return null;
  }

  const loadingView = (
    <Loading>
      {creatingText}{" "}
      <TypewriterText typingDelayMs={500}>{loadingText}</TypewriterText>
    </Loading>
  );

  const gameView = (
    <AppColumnGrid>
      <CopyCodeButton />
      <GameCode />
      <StartGameButton />
      <GameWaitingRoomPlayers />
    </AppColumnGrid>
  );

  return (
    <Container>
      <ColumnGrid>
        {appStatus !== "game-not-started" && loadingView}
        {appStatus === "game-not-started" && gameView}
      </ColumnGrid>
    </Container>
  );
}
