import styled from "styled-components";
import { AppThemeProps } from "../globalStyles";

export const PrimaryButton = styled.button`
  ${(props: AppThemeProps) => props.theme.fontMainCss}

  font-size: 1.5rem;
  @media only screen and (min-width: 768px) {
    font-size: 2rem;
  }
  font-weight: 400;
  line-height: 1;
  text-transform: uppercase;
  padding: 0.5rem 0.75rem 0.5rem 0.75rem;

  background: ${(props: AppThemeProps) =>
    props.theme.colorPrimaryButton.normal};
  box-shadow: 2px 2px 10px
    ${(props: AppThemeProps) => props.theme.colorPrimaryButton.shadow};

  &:focus {
    outline: 0px solid transparent;
    background: ${(props: AppThemeProps) =>
      props.theme.colorPrimaryButton.focused};
  }

  &:active {
    background: ${(props: AppThemeProps) =>
      props.theme.colorPrimaryButton.pressed};
    box-shadow: 2px 2px 10px
      ${(props: AppThemeProps) => props.theme.colorPrimaryButton.pressedShadow};
  }
`;

export const SecondaryButton = styled(PrimaryButton)`
  background: ${(props: AppThemeProps) =>
    props.theme.colorSecondaryButton.normal};
  box-shadow: 2px 2px 10px
    ${(props: AppThemeProps) => props.theme.colorSecondaryButton.shadow};

  &:focus {
    outline: 0px solid transparent;
    background: ${(props: AppThemeProps) =>
      props.theme.colorSecondaryButton.focused};
  }

  &:active {
    background: ${(props: AppThemeProps) =>
      props.theme.colorSecondaryButton.pressed};
    box-shadow: 2px 2px 10px
      ${(props: AppThemeProps) =>
        props.theme.colorSecondaryButton.pressedShadow};
  }
`;
