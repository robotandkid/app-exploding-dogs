import React, { useEffect, useState } from "react";
import styled, { css } from "styled-components";
import { AppThemeProps, breakWord } from "../globalStyles";

interface Position {
  loc: number;
  text: string;
}

interface CursorProps {
  startAnimation: boolean;
  stopAnimation: boolean;
}

const Cursor = styled.span<CursorProps>`
  line-height: 0.8;
  font-size: 0.8em;
  font-weight: normal;
  font-style: initial;
  color: ${(props: AppThemeProps) => props.theme.colorBlinkingCursor};
  opacity: 0;

  @keyframes animated-cursor {
    from,
    to {
      opacity: 0;
    }
    50% {
      opacity: 1;
    }
  }

  ${(props) =>
    props.startAnimation && !props.stopAnimation
      ? css`
          animation: animated-cursor 1000ms infinite;
        `
      : ""}
`;

const Text = styled.span`
  // display: block;
`;

export function TypewriterText(props: {
  className?: string;
  children?: string;
  startTyping?: boolean;
  typingDelayMs: number;
  onDone?: (done: boolean) => void;
}) {
  const [current, setCurrent] = useState<Position>({ loc: 0, text: "" });

  const text = props.children || "";
  const tickDuration = props.typingDelayMs;
  const done = current.loc === text.length;

  const start =
    typeof props.startTyping === "undefined" ? true : props.startTyping;

  useEffect(() => {
    function update() {
      setCurrent((cur) =>
        cur.loc < text.length
          ? {
              loc: cur.loc + 1,
              text: text.substring(0, cur.loc + 1),
            }
          : cur
      );
    }

    let unsubscribe: number | undefined;

    if (start) {
      unsubscribe = setInterval(
        () => requestAnimationFrame(update),
        tickDuration
      );
    }

    if (done) {
      clearInterval(unsubscribe);
    }

    return () => clearInterval(unsubscribe);
  }, [done, start, text, tickDuration]);

  useEffect(() => {
    if (done) {
      requestAnimationFrame(() => props.onDone?.(true));
    }
  }, [done, props.onDone]);

  return (
    <span className={props.className}>
      {current.text}
      <Cursor startAnimation={start} stopAnimation={done}>
        |
      </Cursor>
    </span>
  );
}
