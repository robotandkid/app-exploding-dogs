import React, { useCallback, useEffect, useRef } from "react";
import styled, {
  createGlobalStyle,
  css,
  keyframes,
  ThemeProvider,
} from "styled-components";

const GlobalStyle = createGlobalStyle`
    @font-face {
      src: url('/fonts/Sporting_Grotesque-Regular_web.woff2');
      font-family: 'sporting';
    }

    @font-face {
      font-family: 'inter';
      src: url('/fonts/Inter.otf') format('opentype');
      font-optical-sizing: auto;
      font-weight: 100 1000;
      font-style: normal;
    } 

    body {
      font-family: sans-serif;
      font-size: 62.5%;
    }
`;

const fontMain = css`
  font-family: "inter";
  font-feature-settings: "calt" 1, "case" 1, "ss02" 1;
`;

const pageWidth = css`
  width: 24rem;

  @media only screen and (min-width: 768px) {
    width: 35rem;
  }
  @media only screen and (min-width: 300px) {
    width: 18rem;
  }
`;

const grayTheme = {
  gray1: `hsl(207, 9%, 56%)`,
  gray2: `hsl(205, 7%, 46%)`,
  gray3: `hsl(206, 7%, 40%)`,
  gray4: `hsl(228, 2%, 34%)`,
  gray5: `hsl(228, 3%, 32%)`,
  red: "#c35",
  purple: "#75a",
};

interface ButtonColors {
  normal: string;
  shadow: string;
  focused: string;
  pressed: string;
  pressedShadow: string;
  inactive: string;
}

interface Theme {
  fontMainCss: typeof fontMain;
  colorInputPlaceholder: string;
  colorBlinkingCursor: string;
  colorPrimaryButton: ButtonColors;
  colorSecondaryButton: ButtonColors;
  pageWidth: typeof pageWidth;
  colors: typeof grayTheme;
}

const theme: Theme = {
  fontMainCss: fontMain,
  colorInputPlaceholder: grayTheme.gray2,
  colorBlinkingCursor: grayTheme.gray5,
  colorPrimaryButton: {
    normal: "hsl(207, 0%, 65%)",
    shadow: "hsl(207, 0%, 70%)",
    focused: "hsl(207, 0%, 45%)",
    pressed: "hsl(207, 0%, 65%)",
    pressedShadow: "hsl(207, 0%, 60%)",
    inactive: "hsl(207, 0%, 95%)",
  },
  colorSecondaryButton: {
    normal: "hsl(207, 0%, 95%)",
    shadow: "hsl(207, 0%, 70%)",
    focused: "hsl(207, 0%, 65%)",
    pressed: "hsl(207, 0%, 95%)",
    pressedShadow: "hsl(207, 0%, 60%)",
    inactive: "hsl(207, 0%, 95%)",
  },
  pageWidth,
  colors: grayTheme,
};

export interface AppThemeProps {
  theme: Theme;
}

const AppThemeProvider = (props: { children?: React.ReactNode }) => {
  return <ThemeProvider theme={theme}>{props.children}</ThemeProvider>;
};

export const AppColumnGrid = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-start;
  flex-direction: column;
`;

export const AppRowGrid = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: row;
`;

export const Text1 = styled.h1`
  ${(props: AppThemeProps) => props.theme.fontMainCss}

  font-size: 3.25rem;
  @media only screen and (min-width: 768px) {
    font-size: 4rem;
  }

  font-weight: 1000;
  line-height: 1.2;
  margin: 0;
  padding: 0;
`;

export const Text2 = styled.h2`
  ${(props: AppThemeProps) => props.theme.fontMainCss}

  font-size: 3rem;
  @media only screen and (min-width: 768px) {
    font-size: 3.75rem;
  }
  font-weight: 400;
  line-height: 1.2;
  margin: 0;
  padding: 0;
`;

export const Text3 = styled.h3`
  ${(props: AppThemeProps) => props.theme.fontMainCss}

  font-size: 2.75rem;
  @media only screen and (min-width: 768px) {
    font-size: 3.5rem;
  }
  font-weight: 600;
  line-height: 1.2;
  margin: 0;
  padding: 0;
`;

export const Text4 = styled.h4`
  ${(props: AppThemeProps) => props.theme.fontMainCss}

  font-size: 2.5rem;
  @media only screen and (min-width: 768px) {
    font-size: 3.25rem;
  }
  font-weight: 600;
  line-height: 1.2;
  margin: 0;
  padding: 0;
`;

function InputComp(props: {
  className?: string;
  type: "text";
  /**
   * Set this to true to focus
   */
  focus: boolean;
  value: string;
  onSubmit?: (value: string) => void;
  onChange?: (value: string) => void;
}) {
  const { focus, onSubmit, onChange, value } = props;

  const inputRef = useRef<HTMLInputElement>(null);
  const focusRef = useRef(focus);

  useEffect(() => {
    if (focus && inputRef.current) {
      inputRef.current?.focus();
    }
  }, [focus]);

  useEffect(() => {
    if (focusRef.current) {
      inputRef.current?.focus();
    }
  }, []);

  const _onSubmit = useCallback(
    (e: React.KeyboardEvent<HTMLInputElement>) => {
      if (onSubmit && e.key === "Enter") {
        onSubmit(value);
      }
    },
    [onSubmit, value]
  );

  const _onChange = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      const newValue = e.target.value;

      if (onChange && value !== newValue) {
        onChange(newValue);
      }
    },
    [onChange, value]
  );

  return (
    <input
      ref={inputRef}
      className={props.className}
      value={value}
      onChange={_onChange}
      onKeyDown={_onSubmit}
      type={""}
    />
  );
}

export const Input = styled(InputComp)`
  ${(props: AppThemeProps) => props.theme.fontMainCss}

  border: none;
`;

export const pageTransitionFlyIn = keyframes`
  from {
    opacity: 0;
    filter: blur(4px);
    transform: scale(0);
    transform-origin: center;
  }

  to {
    opacity: 1;
    filter: blur(0);
    transform: scale(1);
    transform-origin: center;
  }
`;

const blinker = keyframes`
@keyframes blinker {
  50% {
    opacity: 0;
  }
}
`;

export const blinkingElem = css`
  animation: ${blinker} 1s linear infinite;
`;

const bounce = keyframes`
  0% {opacity: 1; transform: translateX(0px) scale(1);}
  25%{opacity: 0; transform:translateX(10px) scale(0.9);}
  26%{opacity: 0; transform:translateX(-10px) scale(0.9);}
  55% {opacity: 1; transform: translateX(0px) scale(1);}
`;

export const bounceElem = css`
  animation: ${bounce} 1s linear infinite;
`;

export const breakWord = css`
  word-break: break-word;
  word-wrap: break-word;
  overflow-wrap: break-word;
`;

export const hideOnMobile = css`
  @media only screen and (max-width: 768px) {
    display: none;
  }
`;

export const ApplyStyles = (props: { children?: React.ReactNode }) => (
  <AppThemeProvider>
    <GlobalStyle />
    {props.children}
  </AppThemeProvider>
);
