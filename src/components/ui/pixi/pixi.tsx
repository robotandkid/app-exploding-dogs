import { useRef, useEffect, useState } from "react";
import { Application, Container } from "pixi.js";

interface PixiOpts {
  width: number;
  height: number;
  backgroundColor?: number;
  init?: (app: Application, container: Container) => void;
}

export function usePixi(opts: PixiOpts) {
  const { init } = opts;

  const ref = useRef<HTMLDivElement>(null);
  const initCalled = useRef(false);

  const [pixiApp, setPixiApp] = useState<Application | undefined>();
  const [pixiContainer, setPixiContainer] = useState<Container | undefined>();

  useEffect(
    function create() {
      const instance = new Application({
        width: opts.width,
        height: opts.height,
        backgroundColor: opts.backgroundColor ?? 0xffffff,
        resolution: window.devicePixelRatio || 1,
      });

      ref.current?.appendChild(instance.view);

      const container = new Container();

      instance.stage.addChild(container);

      setPixiApp(instance);
      setPixiContainer(container);
    },
    [opts.backgroundColor, opts.height, opts.width]
  );

  useEffect(
    function initialize() {
      if (pixiApp && pixiContainer && !initCalled.current) {
        init?.(pixiApp, pixiContainer);
        initCalled.current = true;
      }
    },
    [pixiApp, init, pixiContainer]
  );

  return { pixiRef: ref, pixiApp, pixiContainer };
}
