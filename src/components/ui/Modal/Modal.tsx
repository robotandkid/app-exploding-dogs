import ReactDOM from "react-dom";
import styled from "styled-components";
import React, { useState } from "react";
import { ReactNode, useEffect, useRef } from "react";
import { useDispatch } from "react-redux";
import { modalSlice } from "../../../state/appState";

const Screen = styled.div`
  background: black;
  opacity: 0.75;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  position: fixed;
`;

interface StyledModalProps {
  dims: {
    width: string;
    height: string;
  };
}

const StyledModal = styled.div<StyledModalProps>`
  background: white;
  position: fixed;

  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  padding: 2rem;

  @media only screen and (min-width: 768px) {
    width: ${(props) => props.dims.width};
    height: ${(props) => props.dims.height};
    top: 50%;
    left: 50%;
    bottom: auto;
    right: auto;
    transform: translate(-50%, -50%);
  }
`;

export const modalId = "modal-content";

export function Modal(props: {
  onClose: () => void;
  dims: StyledModalProps["dims"];
  children?: ReactNode;
}) {
  const screen = useRef<HTMLDivElement | undefined>();
  const modal = useRef<HTMLDivElement | undefined>();
  const [, setCounter] = useState(0);

  useEffect(function addModalsToDOM() {
    const modalRoot = document.getElementById(modalId);

    screen.current = document.createElement("div");
    modalRoot?.appendChild(screen.current);

    modal.current = document.createElement("div");
    modalRoot?.appendChild(modal.current);

    // fuck me! trigger a re-render!
    setCounter((c) => c + 1);

    return () => {
      modalRoot?.removeChild(screen.current!);
      modalRoot?.removeChild(modal.current!);
    };
  }, []);

  const dispatch = useDispatch();

  useEffect(
    function updateReduxState() {
      dispatch(modalSlice.actions.toggleModalCount({ type: "increment" }));

      return () => {
        // TODO why do I need to add this?
        setTimeout(() =>
          dispatch(modalSlice.actions.toggleModalCount({ type: "decrement" }))
        );
      };
    },
    [dispatch]
  );

  const { onClose } = props;

  useEffect(
    function closeListener() {
      function esc(e: KeyboardEvent) {
        if (e.code === "Escape") {
          onClose();
        }
      }

      window.document.addEventListener("keydown", esc);

      return () => window.document.removeEventListener("keydown", esc);
    },
    [onClose]
  );

  const { dims = { width: "auto", height: "auto" }, children } = props;

  return screen.current && modal.current ? (
    <>
      {ReactDOM.createPortal(<Screen />, screen.current)}
      {ReactDOM.createPortal(
        <StyledModal dims={dims}>{children}</StyledModal>,
        modal.current
      )}
    </>
  ) : null;
}
