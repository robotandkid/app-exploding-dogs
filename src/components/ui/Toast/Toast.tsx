import ReactDOM from "react-dom";
import styled, { createGlobalStyle, css } from "styled-components";
import React, { useState } from "react";
import { ReactNode, useEffect, useRef } from "react";
import { AppThemeProps } from "../globalStyles";

export const toastId = "toast-content";

const ToastCSS = createGlobalStyle`${css`
  #${toastId} > div,
  #${toastId} {
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    position: fixed;
    display: flex;
    align-items: flex-end;
    justify-content: center;
  }
`}`;

const StyledToast = styled.div`
  background-color: hsla(0, 0%, 0%, 0.75);
  border-radius: 10px;
  color: white;
  padding: 1rem;
  ${(props: AppThemeProps) => props.theme.fontMainCss}
  font-size: 1rem;
  font-weight: bold;
  margin-bottom: 1rem;
`;

export const Toast = (props: {
  timeout?: number;
  children?: ReactNode;
  show: boolean;
  onHide: () => void;
}) => {
  const el = useRef<HTMLDivElement | undefined>();
  const [, setCounter] = useState(0);
  const timeout = props.timeout ?? 3000;

  useEffect(() => {
    if (props.show) {
      const root = document.getElementById(toastId);

      el.current = document.createElement("div");
      root?.appendChild(el.current);

      // fuck me! trigger a re-render!
      setCounter((c) => c + 1);

      return () => {
        root?.removeChild(el.current!);
      };
    }
  }, [props.show]);

  const { onHide } = props;

  useEffect(() => {
    function hide() {
      onHide();
    }

    const unsubscribe = setTimeout(() => requestAnimationFrame(hide), timeout);

    return () => clearTimeout(unsubscribe);
  }, [onHide, timeout]);

  return el.current && props.show
    ? ReactDOM.createPortal(
        <>
          <ToastCSS />
          <StyledToast>{props.children}</StyledToast>
        </>,
        el.current
      )
    : null;
};
