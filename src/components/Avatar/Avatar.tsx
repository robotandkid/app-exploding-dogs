import React from "react";

import { Player } from "../../state/appState.types";
import styled from "styled-components";

const Img = styled.img`
  max-width: 100px;
  height: auto;
`;

export function avatarUrl(name: string) {
  return `https://api.adorable.io/avatars/100/${name}.svg`;
}

export function Avatar(props: { player: Player; className?: string }) {
  const src = avatarUrl(props.player.name);

  return <Img className={props.className} alt="" src={src} />;
}
