import React from "react";
import {
  Fixture,
  mockGameNotStartedAppState,
  mockPlayer,
  toFixture,
  Page,
} from "../../../cosmos/cosmos.fixtures";
import { AppStore } from "../../state/appState";
import { GameWaitingRoomPlayers } from "./GameWaitingRoomPlayers";

const fixtures: Fixture[] = [
  {
    store: AppStore({
      app: mockGameNotStartedAppState([
        mockPlayer("1", "Main playa"),
        mockPlayer("2", "Another playa"),
        mockPlayer("3", "reallyreallyreallyreallylooooooongname"),
      ]),
    }),
    name: "GameWaitingRoomPlayers",
  },
];

export default toFixture(
  fixtures,
  <Page>
    <GameWaitingRoomPlayers />
  </Page>
);
