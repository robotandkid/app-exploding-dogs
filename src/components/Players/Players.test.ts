import { numberOfPlayers, NumberOfPlayers } from "../../state/appState.types";
import { __playerRow } from "./Players";
import { mod } from "../../utils/array";
import { arrayDifference } from "../../utils/set";

describe("Players", () => {
  describe("__playerRow", () => {
    function players(count: NumberOfPlayers) {
      return Array.from({ length: count }).map((_, id) => `${id}`);
    }

    function expectedPlayerToLeft(
      currentPlayer: string,
      position: number,
      totalPlayers: number
    ) {
      return mod(+currentPlayer - position, totalPlayers);
    }

    function expectedPlayerToRight(
      currentPlayer: string,
      position: number,
      totalPlayers: number
    ) {
      return mod(+currentPlayer + position, totalPlayers);
    }

    describe("all cases", () => {
      const startingPosition = numberOfPlayers.flatMap((playerCount) => {
        return Array.from({ length: playerCount }).map((_, currentPlayer) => ({
          players: players(playerCount),
          currentPlayer: `${currentPlayer}`,
        }));
      });
      const cases: Array<
        Parameters<typeof __playerRow>[0]
      > = startingPosition.map((_case) => ({
        ..._case,
        gameDirection: "normal",
      }));

      cases.forEach(({ players, currentPlayer, gameDirection }) => {
        describe(`when there are ${players.length} players and currentPlayer=${currentPlayer}`, () => {
          const { playersToLeft, playersToRight } = __playerRow({
            currentPlayer,
            players,
            gameDirection,
          });

          it(`players to the left are in the right order (${gameDirection} order)`, () => {
            playersToLeft?.forEach((player, i) => {
              expect(+player).toEqual(
                expectedPlayerToLeft(currentPlayer, i + 1, players.length)
              );
            });
          });

          it(`players to the right are in the right order (${gameDirection} order)`, () => {
            playersToRight?.forEach((player, i) => {
              expect(+player).toEqual(
                expectedPlayerToRight(currentPlayer, i + 1, players.length)
              );
            });
          });

          it(`players to the left do not have duplicates`, () => {
            const a = playersToLeft?.reduce((total, cur) => {
              if (!total[cur]) {
                total[cur] = 1;
              } else {
                ++total[cur];
              }
              return total;
            }, {} as { [key: string]: number });

            expect(Object.values(a || {})).toEqual(
              Array.from({ length: playersToLeft?.length || 0 }).map(() => 1)
            );
          });

          it(`players to the right do not have duplicates`, () => {
            const a = playersToRight?.reduce((total, cur) => {
              if (!total[cur]) {
                total[cur] = 1;
              } else {
                ++total[cur];
              }
              return total;
            }, {} as { [key: string]: number });

            expect(Object.values(a || {})).toEqual(
              Array.from({ length: playersToRight?.length || 0 }).map(() => 1)
            );
          });

          it(`players left/right do not overlap`, () => {
            expect(
              arrayDifference(playersToLeft || [], playersToRight || [])
            ).toEqual(new Set(playersToLeft));
            expect(
              arrayDifference(playersToRight || [], playersToLeft || [])
            ).toEqual(new Set(playersToRight));
          });

          it(`players left/right add up to players`, () => {
            expect(
              (playersToRight?.length || 0) + (playersToLeft?.length || 0) + 1
            ).toEqual(players.length);
          });
        });
      });
    });
  });
});
