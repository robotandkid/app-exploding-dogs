import React from "react";
import {
  Fixture,
  mockPlayer,
  mockPlayGameState,
  toFixture,
} from "../../../cosmos/cosmos.fixtures";
import { AppStore } from "../../state/appState";
import { Players } from "./Players";

const fixtures: Fixture[] = [
  {
    name: "2 Player (normal direction)",
    store: AppStore({
      app: mockPlayGameState({
        players: [
          mockPlayer("1", "moonshine"),
          mockPlayer("2", "saharareallyreallylong"),
        ],
        currentPlayerId: "2",
        direction: "normal",
      }),
    }),
  },
  {
    name: "2 Player (reverse direction)",
    store: AppStore({
      app: mockPlayGameState({
        players: [
          mockPlayer("1", "moonshine"),
          mockPlayer("2", "saharareallyreallylong"),
        ],
        currentPlayerId: "2",
        direction: "reverse",
      }),
    }),
  },
  {
    name: "3 Player (normal direction)",
    store: AppStore({
      app: mockPlayGameState({
        players: [
          mockPlayer("1", "moonshine"),
          mockPlayer("2", "saharareallyreallylong"),
          mockPlayer("3", "bullwinkle"),
        ],
        currentPlayerId: "2",
        direction: "normal",
      }),
    }),
  },
  {
    name: "3 Player (reverse direction)",
    store: AppStore({
      app: mockPlayGameState({
        players: [
          mockPlayer("1", "moonshine"),
          mockPlayer("2", "saharareallyreallylong"),
          mockPlayer("3", "bullwinkle"),
        ],
        currentPlayerId: "2",
        direction: "reverse",
      }),
    }),
  },
  {
    name: "4 Player (normal direction)",
    store: AppStore({
      app: mockPlayGameState({
        players: [
          mockPlayer("1", "moonshine"),
          mockPlayer("2", "saharareallyreallylong"),
          mockPlayer("3", "bullwinkle"),
          mockPlayer("4", "fireball"),
        ],
        currentPlayerId: "2",
        direction: "normal",
      }),
    }),
  },
  {
    name: "4 Player (reverse direction)",
    store: AppStore({
      app: mockPlayGameState({
        players: [
          mockPlayer("1", "moonshine"),
          mockPlayer("2", "saharareallyreallylong"),
          mockPlayer("3", "bullwinkle"),
          mockPlayer("4", "fireball"),
        ],
        currentPlayerId: "2",
        direction: "reverse",
      }),
    }),
  },
];

export default toFixture(fixtures, <Players />);
