import React, { useMemo } from "react";
import { useSelector } from "react-redux";
import styled from "styled-components";
import { getGamePlayersMap, getPlayer, RootState } from "../../state/appState";
import { Player } from "../../state/appState.types";
import { Avatar } from "../Avatar/Avatar";
import {
  AppColumnGrid,
  AppRowGrid,
  AppThemeProps,
  Text4,
  breakWord,
} from "../ui/globalStyles";

const PlayersTitle = styled(Text4)`
  margin-bottom: 1rem;
`;

const PlayerPicture = styled(Avatar)`
  max-height: 2rem;
  @media only screen and (min-width: 768px) {
    max-height: 3rem;
  }
`;

const PlayerName = styled.div`
  ${(props: AppThemeProps) => props.theme.fontMainCss}

  font-size: 1.5rem;
  @media only screen and (min-width: 768px) {
    font-size: 2.5rem;
  }

  margin-left: 0.5rem;
  ${breakWord}
`;

const PlayersColumnGrid = styled(AppColumnGrid)`
  align-items: flex-start;
`;

const PlayerRowGrid = styled(AppRowGrid)`
  justify-content: flex-start;
  align-content: flex-start;
  align-items: flex-start;
  margin-top: 0.5rem;
`;

export function GameWaitingRoomPlayers() {
  const playersMap = useSelector<
    RootState,
    { [id: string]: Player } | undefined
  >(getGamePlayersMap);
  const player = useSelector<RootState, Player | undefined>(getPlayer);
  const players: Player[] = useMemo(() => {
    if (!player) {
      return [];
    }

    const _map = { ...playersMap } as { [id: string]: Player };

    delete _map[player.id as any];

    const players: Player[] = Object.values(_map);

    return [player, ...players];
  }, [player, playersMap]);

  if (!player || players.length === 0) {
    return null;
  }

  return (
    <AppColumnGrid>
      <PlayersTitle>Players</PlayersTitle>

      <PlayersColumnGrid>
        {players.map((player) => (
          <PlayerRowGrid key={player.id}>
            <PlayerPicture player={player} />
            <PlayerName>{player.name}</PlayerName>
          </PlayerRowGrid>
        ))}
      </PlayersColumnGrid>
    </AppColumnGrid>
  );
}
