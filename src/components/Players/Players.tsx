import React from "react";
import { useSelector } from "react-redux";
import styled, { css } from "styled-components";
import {
  getAppStatus,
  getGameCurrentPlayerId,
  getGameDirection,
  getGamePlayerOrderById,
  getGamePlayersMap,
  RootState,
} from "../../state/appState";
import {
  AppStatus,
  GameStarted,
  NumberOfPlayers,
  Player as PlayerType,
  PlayGameAppState,
} from "../../state/appState.types";
import { circularArray } from "../../utils/array";
import { Avatar } from "../Avatar/Avatar";
import {
  AppColumnGrid,
  AppRowGrid,
  AppThemeProps,
  bounceElem,
  breakWord,
  hideOnMobile,
} from "../ui/globalStyles";

const PlayerPicture = styled(Avatar)`
  max-height: 2rem;
  @media only screen and (min-width: 768px) {
    max-height: 3rem;
  }
`;

const PlayerName = styled.div`
  ${(props: AppThemeProps) => props.theme.fontMainCss}

  font-size: 1rem;
  max-width: 7.5rem;
  margin-left: 0.5rem;
  margin-right: 0.5rem;

  @media only screen and (min-width: 768px) {
    font-size: 1.5rem;
    max-width: 8rem;
    margin-left: 1rem;
    margin-right: 1rem;
  }

  ${breakWord}
`;

function Player(props: { className?: string; player: PlayerType }) {
  return (
    <AppColumnGrid className={props.className}>
      <PlayerPicture player={props.player} />
      <PlayerName>{props.player.name}</PlayerName>
    </AppColumnGrid>
  );
}

const OtherPlayer = styled(Player)<{ hideOnMobile: boolean }>`
  opacity: 0.25;

  ${(props) =>
    props.hideOnMobile &&
    css`
      ${hideOnMobile}
    `}
`;

function OtherPlayers(props: { playerIds: string[] | undefined }) {
  const playersMap = useSelector<
    RootState,
    { [id: string]: PlayerType } | undefined
  >(getGamePlayersMap);

  const players: PlayerType[] | undefined = props.playerIds
    ?.map((id) => playersMap?.[id]!)
    .filter((player) => !!player);

  return (
    <>
      {players?.map((player, index) => (
        <OtherPlayer
          hideOnMobile={index > 0 ? true : false}
          player={player!}
          key={player!.id}
        />
      ))}
    </>
  );
}

const CurrentPlayer = styled(Player)`
  border: 0.25rem solid ${(props: AppThemeProps) => props.theme.colors.red};
  padding: 0.5rem;
  margin-left: 0.5rem;
  margin-right: 0.5rem;
`;

const Direction = styled.div`
  ${(props: AppThemeProps) => props.theme.fontMainCss}

  font-size: 1.5rem;
  @media only screen and (min-width: 768px) {
    font-size: 2rem;
  }
  font-weight: bold;

  color: ${(props: AppThemeProps) => props.theme.colors.red};

  ${bounceElem}
`;

/**
 * Returns the players to the left and to the right of the current player
 *
 * @param props
 */
export function __playerRow(props: {
  currentPlayer: string;
  gameDirection: PlayGameAppState["game"]["direction"];
  players: string[];
}) {
  const {
    currentPlayer: currentPlayerId,
    gameDirection,
    players: playerOrderById,
  } = props;

  let playersToLeft: string[] | undefined;
  let playersToRight: string[] | undefined;

  const playerCount = (playerOrderById.length as any) as NumberOfPlayers;
  const currentPlayerPos = playerOrderById.indexOf(currentPlayerId);

  switch (playerCount) {
    case 2:
      if (gameDirection === "normal") {
        playersToRight = [circularArray(playerOrderById, currentPlayerPos + 1)];
      } else {
        playersToLeft = [circularArray(playerOrderById, currentPlayerPos + 1)];
      }
      break;
    case 3:
      playersToRight = [circularArray(playerOrderById, currentPlayerPos + 1)];
      playersToLeft = [circularArray(playerOrderById, currentPlayerPos - 1)];
      break;
    case 4:
      playersToRight = [
        circularArray(playerOrderById, currentPlayerPos + 1),
        circularArray(playerOrderById, currentPlayerPos + 2),
      ];
      playersToLeft = [circularArray(playerOrderById, currentPlayerPos - 1)];
      break;
    case 5:
      playersToRight = [
        circularArray(playerOrderById, currentPlayerPos + 1),
        circularArray(playerOrderById, currentPlayerPos + 2),
      ];
      playersToLeft = [
        circularArray(playerOrderById, currentPlayerPos - 1),
        circularArray(playerOrderById, currentPlayerPos - 2),
      ];
      break;
    default: {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const x: never = playerCount;
      throw new Error(`Woah: there an unexpected number of players ${x}`);
    }
  }

  return {
    playersToLeft,
    playersToRight,
  };
}

const RightArrow = () => <Direction>&raquo;</Direction>;
const LeftArrow = () => <Direction>&laquo;</Direction>;

export function Players() {
  const playersMap = useSelector<
    RootState,
    { [id: string]: PlayerType } | undefined
  >(getGamePlayersMap);

  const currentPlayer = useSelector<RootState, string | undefined>(
    getGameCurrentPlayerId
  );
  const players = useSelector<RootState, string[] | undefined>(
    getGamePlayerOrderById
  );
  const gameDirection = useSelector<
    RootState,
    GameStarted["direction"] | undefined
  >(getGameDirection);
  const appStatus = useSelector<RootState, AppStatus>(getAppStatus);

  if (
    appStatus !== "play-game" ||
    !currentPlayer ||
    !players ||
    !playersMap ||
    !gameDirection
  ) {
    return null;
  }

  const currentPlayerType: PlayerType = playersMap[currentPlayer];

  const { playersToLeft, playersToRight } = __playerRow({
    currentPlayer: currentPlayer,
    players: players,
    gameDirection,
  });

  return (
    <AppRowGrid>
      <OtherPlayers playerIds={playersToLeft} />
      {gameDirection === "reverse" && <LeftArrow />}
      <CurrentPlayer player={currentPlayerType} key={currentPlayerType.id} />
      {gameDirection === "normal" && <RightArrow />}
      <OtherPlayers playerIds={playersToRight} />
    </AppRowGrid>
  );
}
