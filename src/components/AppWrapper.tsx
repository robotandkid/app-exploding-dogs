import React, { ReactNode } from "react";
import { Provider } from "react-redux";
import { Store } from "redux";
import { ApplyStyles } from "./ui/globalStyles";

export function AppWrapper(props: {
  store: Store<any, any>;
  children?: ReactNode;
}) {
  return (
    <Provider store={props.store}>
      <ApplyStyles>{props.children}</ApplyStyles>
    </Provider>
  );
}
