import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";
import { appSlice, getAppStatus, RootState } from "./appState";
import { AppStatus } from "./appState.types";

export const route = [
  "/",
  "/lobby",
  "/game-waiting-room",
  "/play",
  "/watch",
  "/gameover",
] as const;

export type Route = typeof route[number];

export function useAppRouter() {
  const { push, replace, pathname, query } = useRouter() || {};

  return {
    pathname: pathname as Route,
    query,
    push(pathname: Route, _query: typeof query, opts?: { shallow?: boolean }) {
      push?.({ pathname, query: { ..._query } }, undefined, opts);
    },
    replace(
      pathname: Route,
      _query: typeof query,
      opts?: { shallow?: boolean }
    ) {
      replace?.({ pathname, query: { ..._query } }, undefined, opts);
    },
  };
}

function useEnsureOnCorrectRoute() {
  const appStatus = useSelector<RootState, AppStatus>(getAppStatus);
  const dispatch = useDispatch();
  const { push, pathname, query } = useAppRouter();

  if (typeof window === "undefined") {
    return;
  }

  switch (appStatus) {
    case "init":
    case "try-to-load-player":
    case "try-to-create-player":
    case "player-loaded":
    case "player-registration":
      if (pathname !== "/") {
        push("/", query, { shallow: true });
      }
      break;
    case "lobby":
      if (!!query.code) {
        dispatch(
          appSlice.actions.goToGameWaitingRoom({
            prevStatus: appStatus,
          })
        );

        push("/game-waiting-room", query, { shallow: true });
      }
      if (pathname !== "/lobby") {
        push("/lobby", query, { shallow: true });
      }
      break;
    case "game-waiting-room":
    case "try-to-create-game":
    case "try-to-join-game":
    case "game-not-started":
      if (pathname !== "/game-waiting-room") {
        push("/game-waiting-room", query, { shallow: true });
      }
      break;
    case "play-game":
      if (pathname !== "/play") {
        push("/play", query, { shallow: true });
      }
      break;
    case "watch-mode":
      if (pathname !== "/watch") {
        push("/watch", query, { shallow: true });
      }
      break;
    case "game-over":
      break;
    default:
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const x: never = appStatus;
  }
}

export const EnsureOnCorrectRoute = () => {
  useEnsureOnCorrectRoute();

  return null;
};
