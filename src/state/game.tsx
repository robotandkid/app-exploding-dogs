import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  createGame,
  listenToGameState,
  loadPlayers,
  tryToJoinGame,
} from "../firebase/firebase";
import { arrayDifference } from "../utils/set";
import {
  appSlice,
  appStore,
  getAppStatus,
  getGameCode,
  getGamePlayersMap,
  getPlayer,
  RootState,
} from "./appState";
import {
  AppStatus,
  GameState,
  isGameNotStarted,
  Player,
} from "./appState.types";
import { useAppRouter } from "./route";

async function loadPlayersWhenDifferent(
  newPlayerIds: string[],
  oldPlayerIds: string[] | undefined
) {
  if (!oldPlayerIds) {
    return;
  }

  const playersChanged =
    arrayDifference(newPlayerIds, oldPlayerIds).size !== 0 ||
    arrayDifference(oldPlayerIds, newPlayerIds).size !== 0;

  if (!playersChanged) {
    return;
  }

  console.info("loadPlayers");

  const players = await loadPlayers(newPlayerIds);

  appStore.dispatch(appSlice.actions.syncPlayers({ players }));
}

export function ListenToGameState() {
  const gameCode = useSelector<RootState, string | undefined>(getGameCode);
  const player = useSelector<RootState, Player | undefined>(getPlayer);
  const playersMap = useSelector<
    RootState,
    { [id: string]: Player } | undefined
  >(getGamePlayersMap);
  const dispatch = useDispatch();

  useEffect(
    function _listenToGameState() {
      if (!gameCode || !player) {
        // A game code from the app state means a game was loaded.
        return;
      }

      console.info("listenToGameState");

      const unsubscribe = listenToGameState(
        gameCode,
        player,
        (newGame, newPlayers) => {
          appStore.dispatch(appSlice.actions.syncGameState({ game: newGame }));

          const playerIds = playersMap ? Object.keys(playersMap) : undefined;

          loadPlayersWhenDifferent(newPlayers, playerIds);
        }
      );

      return () => unsubscribe();
    },
    // All we care about here is the gameCode .
    // If we also listen for playerMap changes,
    // then we'll unnecessarily recreate the game state listener.
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [gameCode, player, dispatch]
  );

  return null;
}

export function CreateOrLoadGame() {
  const dispatch = useDispatch();
  const player = useSelector<RootState, Player | undefined>(getPlayer)!;
  const appStatus = useSelector<RootState, AppStatus>(getAppStatus);
  const { query, replace, pathname } = useAppRouter();
  const [game, setGame] = useState<GameState | undefined>();

  useEffect(() => {
    if (appStatus === "game-waiting-room" && !query.code) {
      dispatch(
        appSlice.actions.goToGameCreation({
          prevStatus: appStatus,
        })
      );
      createGame(player).then((game) => setGame(game));
    } else if (appStatus === "game-waiting-room" && query.code) {
      dispatch(
        appSlice.actions.goToGameLoad({
          prevStatus: appStatus,
        })
      );
      tryToJoinGame(query.code as string, player).then(setGame);
    }
  }, [appStatus, dispatch, player, query.code]);

  useEffect(() => {
    if (appStatus === "try-to-join-game" && isGameNotStarted(game)) {
      replace(
        pathname,
        { ...query, code: game.gameCode },
        {
          shallow: true,
        }
      );

      dispatch(
        appSlice.actions.goToGameNotStarted({
          prevStatus: appStatus,
          game,
        })
      );
    } else if (appStatus === "try-to-create-game" && isGameNotStarted(game)) {
      replace(
        pathname,
        { ...query, code: game.gameCode },
        {
          shallow: true,
        }
      );

      dispatch(
        appSlice.actions.goToGameCreated({
          prevStatus: appStatus,
          game,
        })
      );
    }
  }, [appStatus, dispatch, game, pathname, query, replace]);

  return null;
}
