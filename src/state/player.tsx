import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  addPlayer,
  getPlayer as getPlayerFromServer,
} from "../firebase/firebase";
import {
  appSlice,
  getAppStatus,
  getPlayerIdToLoad,
  getPlayerNameToLoad,
  RootState,
} from "./appState";
import { AppStatus, Player } from "./appState.types";

function getPlayerFromSessionStorage() {
  if (typeof sessionStorage !== "undefined") {
    return sessionStorage.getItem("playerId");
  }
  return undefined;
}

function savePlayerToSessionStorage(player: Player) {
  sessionStorage.setItem("playerId", player.id);
}

function useTryToadPlayer() {
  const dispatch = useDispatch();
  const appStatus: AppStatus = useSelector<RootState, AppStatus>(getAppStatus);
  const playerIdToLoad = useSelector<RootState, string | undefined>(
    getPlayerIdToLoad
  );

  useEffect(
    function tryToLoadPlayerOnInit() {
      if (appStatus !== "init") {
        return;
      }

      const playerIdToLoad = getPlayerFromSessionStorage();

      if (playerIdToLoad) {
        dispatch(
          appSlice.actions.goToPlayerLoad({
            prevStatus: appStatus,
            playerIdToLoad,
          })
        );
      } else {
        dispatch(
          appSlice.actions.goToPlayerRegistration({
            prevStatus: appStatus,
          })
        );
      }
    },
    [appStatus, dispatch]
  );

  useEffect(
    function tryToLoadPlayer() {
      if (appStatus !== "try-to-load-player" || !playerIdToLoad) {
        return;
      }
      getPlayerFromServer(playerIdToLoad).then((player) => {
        if (player) {
          dispatch(
            appSlice.actions.goToPlayerLoaded({
              prevStatus: appStatus,
              player,
            })
          );
        } else {
          dispatch(
            appSlice.actions.goToPlayerRegistrationFromPlayerLoad({
              prevStatus: appStatus,
            })
          );
        }
      });
    },
    [appStatus, dispatch, playerIdToLoad]
  );
}

export function TryToLoadPlayer() {
  useTryToadPlayer();

  return null;
}

function useTryToCreatePlayer() {
  const dispatch = useDispatch();
  const appStatus = useSelector<RootState, AppStatus>(getAppStatus);
  const playerNameToLoad = useSelector<RootState, string | undefined>(
    getPlayerNameToLoad
  );
  const [name, setName] = useState<string | undefined>();

  useEffect(() => {
    if (appStatus !== "player-registration" || !name) {
      return;
    }

    dispatch(
      appSlice.actions.tryToCreatePlayer({
        prevStatus: appStatus,
        playerToLoad: name,
      })
    );
  }, [appStatus, dispatch, name]);

  useEffect(() => {
    if (appStatus !== "try-to-create-player" || !playerNameToLoad) {
      return;
    }

    addPlayer(playerNameToLoad).then((player) => {
      savePlayerToSessionStorage(player);
      dispatch(
        appSlice.actions.goToLobbyFromPlayerCreate({
          player,
          prevStatus: appStatus,
        })
      );
    });
  }, [appStatus, dispatch, playerNameToLoad]);

  return [name, setName] as [string | undefined, (name: string) => void];
}

export function TryToCreatePlayer() {
  useTryToCreatePlayer();

  return null;
}
