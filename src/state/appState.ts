import { configureStore, createSlice, PayloadAction } from "@reduxjs/toolkit";
import {
  AppState,
  AppStatus,
  GameNotStarted,
  GameNotStartedAppState,
  GameOverAppState,
  isGameNotStarted,
  isGameOver,
  isGameStarted,
  PartialGameState,
  Player,
  PlayGameAppState,
  numberOfPlayers,
  NumberOfPlayers,
} from "./appState.types";

interface StateTransitions {
  init: "try-to-load-player" | "player-registration";
  "try-to-load-player": "player-loaded" | "player-registration";
  /**
   * Can bypass lobby if has code
   */
  "player-loaded": "lobby" | "game-waiting-room";
  "player-registration": "try-to-create-player";
  /**
   * Can bypass lobby if has code
   */
  "try-to-create-player": "lobby" | "game-waiting-room";
  lobby: "game-waiting-room";
  "game-waiting-room": "try-to-create-game" | "try-to-join-game";
  "try-to-create-game": "game-not-started";
  "try-to-join-game":
    | "game-not-started"
    | "play-game"
    | "watch-mode"
    | "game-over";
}

type TransitionStatus = keyof StateTransitions;

type NextTransitionStatus<
  Status extends TransitionStatus
> = StateTransitions[Status];

type AppStateSlice<Status extends AppStatus> = AppState & {
  status: Status;
};

type PrevAppState<PrevStatus extends TransitionStatus> = AppStateSlice<
  PrevStatus
>;

function stateTransition<
  Payload,
  PrevStatus extends TransitionStatus,
  NextStatus extends NextTransitionStatus<PrevStatus>
>(
  prevState: PrevStatus,
  nextState: NextStatus,
  callback: (
    state: PrevAppState<PrevStatus>,
    payload: PayloadAction<Payload>
  ) => AppStateSlice<NextStatus>
) {
  return (
    state: AppState,
    payload: PayloadAction<Payload & { prevStatus: PrevStatus }>
  ): AppState => {
    const newState = callback(state as PrevAppState<PrevStatus>, payload);

    return {
      ...state,
      ...newState,
    };
  };
}

export const appSlice = createSlice({
  name: "appState",
  initialState: {
    player: undefined,
    game: undefined,
    status: "init",
  } as AppState,
  reducers: {
    goToPlayerLoad: stateTransition(
      "init",
      "try-to-load-player",
      (state, action: PayloadAction<{ playerIdToLoad: string }>) => ({
        ...state,
        player: undefined,
        game: undefined,
        status: "try-to-load-player",
        playerIdToLoad: action.payload.playerIdToLoad,
      })
    ),
    goToPlayerRegistration: stateTransition(
      "init",
      "player-registration",
      (state, action: PayloadAction<{}>) => ({
        ...state,
        player: undefined,
        game: undefined,
        status: "player-registration",
      })
    ),
    goToPlayerLoaded: stateTransition(
      "try-to-load-player",
      "player-loaded",
      (state, action: PayloadAction<{ player: Player }>) => ({
        ...state,
        game: undefined,
        player: action.payload.player,
        status: "player-loaded",
      })
    ),
    goToLobbyFromPlayerLoaded: stateTransition(
      "player-loaded",
      "lobby",
      (state, action: PayloadAction<{}>) => ({
        ...state,
        game: undefined,
        player: state.player as Player,
        status: "lobby",
      })
    ),
    goToLobbyFromPlayerCreate: stateTransition(
      "try-to-create-player",
      "lobby",
      (state, action: PayloadAction<{ player: Player }>) => ({
        ...state,
        game: undefined,
        player: action.payload.player,
        status: "lobby",
      })
    ),
    goToPlayerRegistrationFromPlayerLoad: stateTransition(
      "try-to-load-player",
      "player-registration",
      (state, action: PayloadAction<{}>) => ({
        ...state,
        game: undefined,
        player: undefined,
        status: "player-registration",
      })
    ),
    tryToCreatePlayer: stateTransition(
      "player-registration",
      "try-to-create-player",
      (state, action: PayloadAction<{ playerToLoad: string }>) => ({
        ...state,
        player: undefined,
        game: undefined,
        status: "try-to-create-player",
        playerToLoad: action.payload.playerToLoad,
      })
    ),
    goToGameWaitingRoom: stateTransition(
      "lobby",
      "game-waiting-room",
      (state, action: PayloadAction<{}>) => ({
        ...state,
        game: undefined,
        player: state.player as Player,
        status: "game-waiting-room",
      })
    ),
    goToGameCreation: stateTransition(
      "game-waiting-room",
      "try-to-create-game",
      (state, action: PayloadAction<{}>) => ({
        ...state,
        game: undefined,
        player: state.player as Player,
        status: "try-to-create-game",
      })
    ),
    goToGameLoad: stateTransition(
      "game-waiting-room",
      "try-to-join-game",
      (state, action: PayloadAction<{}>) => ({
        ...state,
        game: undefined,
        player: state.player as Player,
        status: "try-to-join-game",
      })
    ),
    goToGameNotStarted: stateTransition(
      "try-to-join-game",
      "game-not-started",
      (state, action: PayloadAction<{ game: GameNotStarted }>) => ({
        ...state,
        game: action.payload.game,
        player: state.player as Player,
        status: "game-not-started",
      })
    ),
    goToGameCreated: stateTransition(
      "try-to-create-game",
      "game-not-started",
      (state, action: PayloadAction<{ game: GameNotStarted }>) => ({
        ...state,
        game: action.payload.game,
        player: state.player as Player,
        status: "game-not-started",
      })
    ),
    syncGameState(state, action: PayloadAction<{ game: PartialGameState }>) {
      if (
        state.status !== "game-not-started" &&
        state.status !== "play-game" &&
        state.status !== "game-over"
      ) {
        return state;
      }

      const { game } = action.payload;
      const { player } = state;

      if (isGameNotStarted(game)) {
        return {
          ...state,
          game: {
            ...game,
            players: state.game.players,
            watchers: state.game.watchers,
          },
          status: "game-not-started",
        };
      }

      if (isGameStarted(game) && game.players[player.id]) {
        return {
          ...state,
          game: {
            ...game,
            players: state.game.players,
            watchers: state.game.watchers,
          },
          status: "play-game",
        };
      }

      if (isGameStarted(game) && !!game.players[player.id]) {
        return {
          ...state,
          game: {
            ...game,
            players: state.game.players,
            watchers: state.game.watchers,
          },
          status: "watch-mode",
        };
      }

      if (isGameOver(game)) {
        return {
          ...state,
          game: {
            ...game,
            players: state.game.players,
            watchers: state.game.watchers,
          },
          status: "game-over",
        };
      }

      return state;
    },
    syncPlayers(
      state,
      action: PayloadAction<{
        players: Player[];
      }>
    ) {
      if (
        state.status !== "game-not-started" &&
        state.status !== "play-game" &&
        state.status !== "game-over"
      ) {
        return state;
      }

      return {
        ...state,
        game: {
          ...state.game,
          players: action.payload.players.reduce(
            (total, cur) => ({ ...total, [cur.id]: cur }),
            {}
          ),
        },
      } as GameNotStartedAppState | PlayGameAppState | GameOverAppState;
    },
  },
});

export const modalSlice = createSlice({
  name: "openModals",
  initialState: 0,
  reducers: {
    toggleModalCount: (
      state,
      action: PayloadAction<{
        type: "increment" | "decrement";
      }>
    ) => (action.payload.type === "increment" ? state + 1 : state - 1),
  },
});

export const AppStore = (initialState?: {
  app?: AppState;
  openModals?: number;
}) =>
  configureStore({
    reducer: {
      app: appSlice.reducer,
      openModals: modalSlice.reducer,
    },
    preloadedState: initialState,
  });

export const appStore = AppStore();

export type RootState = ReturnType<typeof appStore.getState>;

export type AppDispatch = typeof appStore.dispatch;

export const getOpenModals = (state: RootState) => state.openModals;

export const getPlayer = (state: RootState) => state.app.player;

export const getAppStatus = (state: RootState) => state.app.status;

export const getPlayerIdToLoad = (state: RootState) =>
  state.app.status === "try-to-load-player"
    ? state.app.playerIdToLoad
    : undefined;

export const getPlayerNameToLoad = (state: RootState) =>
  state.app.status === "try-to-create-player"
    ? state.app.playerToLoad
    : undefined;

/**
 * @deprecated - do NOT use. This will cause re-renders
 * anytime a redux action is dispatched. You most likely
 * want to use property-specific getters.
 *
 * @param state
 */
export const getGameState = (state: RootState) => state.app.game;

export const getGameCode = (state: RootState) => state.app.game?.gameCode;

export const getGamePlayersMap = (state: RootState) => state.app.game?.players;

export const getGameCurrentPlayerId = (state: RootState): string | undefined =>
  isGameStarted(state.app.game) ? state.app.game.currentPlayerId : undefined;

export const getGamePlayerOrderById = (
  state: RootState
): string[] | undefined => state.app.game?.playerOrderById;

export const getGameDirection = (state: RootState) =>
  isGameStarted(state.app.game) ? state.app.game.direction : undefined;

export const getGameNumberOfPlayers = (
  state: RootState
): NumberOfPlayers | undefined => state.app.game?.playerOrderById.length as any;
