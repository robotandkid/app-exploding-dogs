export interface Player {
  name: string;
  id: string;
  active: boolean;
}

export interface BaseGameState {
  id: string;
  startedByPlayerId: string;
  gameCode: string;
  players: { [id: string]: Player };
  watchers: { [id: string]: Player };
  playerOrderById: string[];
  started: boolean;
  done: boolean;
  // playerCards: {
  //   [id in string]: Card[];
  // };
  // deck: Card[];
  // initialDeck: InitialDeck;
}

export interface GameNotStarted extends BaseGameState {
  started: false;
  done: false;
}

export interface GameStarted extends BaseGameState {
  currentPlayerId: string;
  direction: "normal" | "reverse";
  started: true;
  done: false;
}

export interface GameOver extends BaseGameState {
  started: true;
  done: true;
}

export const numberOfPlayers = [2, 3, 4, 5] as const;
export type NumberOfPlayers = typeof numberOfPlayers[number];
export const maxNumberOfPlayers = numberOfPlayers[numberOfPlayers.length - 1];

export interface BaseAppState {}

export interface InitAppState extends BaseAppState {
  player: undefined;
  game: undefined;
  status: "init";
}

export interface PlayerLoadAppState extends BaseAppState {
  player: undefined;
  game: undefined;
  status: "try-to-load-player";
  playerIdToLoad: string;
}

export interface PlayerLoadedAppState extends BaseAppState {
  player: Player;
  game: undefined;
  status: "player-loaded";
}

export interface PlayerCreateAppState extends BaseAppState {
  player: undefined;
  game: undefined;
  status: "try-to-create-player";
  playerToLoad: string;
}

export interface RegisterPlayerAppState extends BaseAppState {
  player: undefined;
  game: undefined;
  status: "player-registration";
}

export interface LobbyAppState extends BaseAppState {
  player: Player;
  game: undefined;
  status: "lobby";
}

export const gameWaitingRoomStatus = [
  /**
   * Player will go to this state from registration/init
   */
  "game-waiting-room",
  /**
   * Player can create a game from waiting room
   */
  "try-to-create-game",
  /**
   * Player can join a game from waiting room
   */
  "try-to-join-game",
  "game-not-started",
] as const;

type GameWaitingRoomStatus = typeof gameWaitingRoomStatus[number];

export interface GameWaitingRoomAppState extends BaseAppState {
  player: Player;
  game: undefined;
  status: Extract<GameWaitingRoomStatus, "game-waiting-room">;
}

export interface GameCreateAppState extends BaseAppState {
  player: Player;
  game: undefined;
  status: Extract<GameWaitingRoomStatus, "try-to-create-game">;
}

export interface GameLoadAppState extends BaseAppState {
  player: Player;
  game: undefined;
  status: Extract<GameWaitingRoomStatus, "try-to-join-game">;
}

export interface GameNotStartedAppState extends BaseAppState {
  player: Player;
  game: GameNotStarted;
  status: Extract<GameWaitingRoomStatus, "game-not-started">;
}

export interface PlayGameAppState extends BaseAppState {
  player: Player;
  game: GameStarted;
  status: "play-game";
}

export interface WatchModeAppState extends BaseAppState {
  player: Player;
  game: GameStarted;
  status: "watch-mode";
}

export interface GameOverAppState extends BaseAppState {
  player: Player;
  game: GameOver;
  status: "game-over";
}

export type AppState =
  | InitAppState
  | PlayerLoadAppState
  | PlayerLoadedAppState
  | PlayerCreateAppState
  | RegisterPlayerAppState
  | LobbyAppState
  | GameCreateAppState
  | GameWaitingRoomAppState
  | GameLoadAppState
  | GameNotStartedAppState
  | PlayGameAppState
  | WatchModeAppState
  | GameOverAppState;

export type AppStatus = Pick<AppState, "status">["status"];

export type GameState = NonNullable<Pick<AppState, "game">["game"]>;

export type PartialGameState = Omit<GameState, "players" | "watchers">;

export function isGameNotStarted(
  g: PartialGameState | undefined
): g is GameNotStarted {
  return !!g && !g.done && !g.started;
}

export function isGameStarted(
  g: PartialGameState | undefined
): g is GameStarted {
  return !!g && !g.done && g.started;
}

export function isGameOver(g: PartialGameState | undefined): g is GameOver {
  return !!g && g.done && g.started;
}
