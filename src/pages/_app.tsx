import App from "next/app";
import React from "react";
import { AppWrapper } from "../components/AppWrapper";
import "../firebase/firebase";
import { appStore } from "../state/appState";
import { ListenToGameState } from "../state/game";
import { TryToLoadPlayer } from "../state/player";
import { EnsureOnCorrectRoute } from "../state/route";

const DataLoaders = () => (
  <>
    <TryToLoadPlayer />
    <ListenToGameState />
    <EnsureOnCorrectRoute />
  </>
);

export default class MyApp extends App {
  render() {
    const { Component, pageProps } = this.props;
    return (
      <AppWrapper store={appStore}>
        <DataLoaders />
        <Component {...pageProps} />
      </AppWrapper>
    );
  }
}
