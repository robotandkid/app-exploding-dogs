import React from "react";
import { Welcome } from "../components/pages/Welcome";
import { TryToCreatePlayer } from "../state/player";

export default function Page() {
  return (
    <>
      <TryToCreatePlayer />
      <Welcome />
    </>
  );
}
