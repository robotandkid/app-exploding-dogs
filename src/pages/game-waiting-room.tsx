import React from "react";
import { GameWaitingRoom } from "../components/pages/GameWaitingRoom";
import { CreateOrLoadGame } from "../state/game";

export default function Page() {
  return (
    <>
      <CreateOrLoadGame />
      <GameWaitingRoom />;
    </>
  );
}
