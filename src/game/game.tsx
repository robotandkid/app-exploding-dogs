import { shuffle } from "../utils/shuffle";
import { Card, CardMeta, dogCard, InitialDeck } from "./game.types";

function cardMeta(): CardMeta[] {
  const dogCardsMeta: CardMeta[] = dogCard.map((kind) => ({
    kind,
    count: 4,
  }));

  return [
    {
      kind: "exploding-dog",
      count: 4,
    } as CardMeta,
    {
      kind: "defuse",
      count: 6,
    } as CardMeta,
    {
      kind: "nope",
      count: 5,
    } as CardMeta,
    {
      kind: "attack",
      count: 4,
    } as CardMeta,
    {
      kind: "skip",
      count: 4,
    } as CardMeta,
    {
      kind: "favor",
      count: 4,
    } as CardMeta,
    {
      kind: "shuffle",
      count: 4,
    } as CardMeta,
    {
      kind: "see-the-future",
      count: 5,
    } as CardMeta,
  ].concat(dogCardsMeta);
}

function generateDeck(cardMeta: Array<CardMeta>): Array<Card> {
  const deck: Array<Card> = [];

  for (let meta of cardMeta) {
    for (let i = 1; i <= meta.count; i++) {
      deck.push({ kind: meta.kind });
    }
  }

  return deck;
}

function shuffleDeck(deck: Array<Card>) {
  return shuffle(deck);
}

export function dealDeck(players: 2 | 3 | 4 | 5): InitialDeck {
  const deck = generateDeck(cardMeta());

  shuffleDeck(deck);

  const bombs = deck
    .filter((card) => card.kind === "exploding-dog")
    .slice(0, players - 1);

  const defuse = deck.filter((card) => card.kind === "defuse");
  const defuseCount = defuse.length;

  const rest = deck.filter(
    (card) => card.kind !== "exploding-dog" && card.kind !== "defuse"
  );

  const playerCards = {} as InitialDeck["cardsToAssignToPlayers"];

  // eslint-disable-next-line array-callback-return
  Array.from({ length: players }, (_, i) => {
    const cards: Card[] = [];

    cards.push(defuse.shift()!);
    // eslint-disable-next-line array-callback-return
    Array.from({ length: 7 }, () => {
      cards.push(rest.shift()!);
    });
    playerCards[
      (i as any) as keyof InitialDeck["cardsToAssignToPlayers"]
    ] = cards;
  });

  const isSmallGame = players === 2 || players === 3;

  const allowedDefused = defuse.slice(
    0,
    isSmallGame ? 2 : defuseCount - players
  );

  const shuffledDeck = shuffleDeck(rest.concat(allowedDefused).concat(bombs));

  return {
    cardsToAssignToPlayers: playerCards,
    remainingDeck: shuffledDeck,
  };
}
