import { NumberOfPlayers } from "../state/appState.types";

export const bombCard = ["exploding-dog", "defuse"] as const;
export type BombCard = typeof bombCard[number];
export const wildCard = ["attack", "favor", "nope"] as const;
export type WildCard = typeof wildCard[number];
export const yogaDefenseCard = ["see-the-future", "skip", "shuffle"] as const;
export type YogaDefenseCard = typeof yogaDefenseCard[number];
export const dogCard = [
  "burrito",
  "papaya",
  "talking-dog",
  "dog-poop",
  "hairy-dog",
] as const;
export type DogCard = typeof dogCard[number];
export type CardKind = DogCard | WildCard | BombCard | YogaDefenseCard;
export interface Card {
  kind: CardKind;
}
export interface InitialDeck {
  cardsToAssignToPlayers: {
    [key in NumberOfPlayers]: Array<Card>;
  };
  remainingDeck: Array<Card>;
}
export interface CardMeta {
  kind: CardKind;
  count: number;
}
