import { NumberOfPlayers } from "../state/appState.types";
import { dealDeck } from "./game";

describe("dealDeck", () => {
  ([2, 3, 4, 5] as NumberOfPlayers[]).forEach((playerCount) => {
    describe(`${playerCount} player games`, () => {
      test(`everyone should have 1 defuse card`, () => {
        const deck = dealDeck(playerCount);

        Array.from({ length: playerCount }, (_, k) => {
          const playerCards = deck.cardsToAssignToPlayers[k as NumberOfPlayers];

          expect(
            playerCards.filter((card) => card.kind === "defuse")
          ).toHaveLength(1);
        });
      });

      test(`deck should have ${
        playerCount <= 3 ? 2 : 6 - playerCount
      } defuse cards`, () => {
        const deck = dealDeck(playerCount);
        const defuseCards = deck.remainingDeck.filter(
          (card) => card.kind === "defuse"
        ).length;

        switch (playerCount) {
          case 2:
          case 3:
            expect(defuseCards).toBe(2);
            break;
          case 4:
            expect(defuseCards).toBe(6 - 4);
            break;
          case 5:
            expect(defuseCards).toBe(6 - 5);
            break;
        }
      });

      test(`no one should have a bomb`, () => {
        const deck = dealDeck(playerCount);

        Array.from({ length: playerCount }, (_, k) => {
          const playerCards = deck.cardsToAssignToPlayers[k as NumberOfPlayers];

          expect(
            playerCards.filter((card) => card.kind === "exploding-dog")
          ).toHaveLength(0);
        });
      });

      test(`deck should have ${playerCount} - 1 bombs`, () => {
        const deck = dealDeck(playerCount);

        expect(
          deck.remainingDeck.filter((card) => card.kind === "exploding-dog")
        ).toHaveLength(playerCount - 1);
      });

      test("everyone should have 8 cards", () => {
        const deck = dealDeck(playerCount);

        Array.from({ length: playerCount }, (_, k) => {
          const playerCards = deck.cardsToAssignToPlayers[k as NumberOfPlayers];

          expect(playerCards).toHaveLength(8);
        });
      });
    });
  });
});
