import { useEffect, EffectCallback, DependencyList, useRef } from "react";

export function useEffectOnce(
  effect: EffectCallback,
  deps?: DependencyList | undefined
) {
  const called = useRef(false);

  useEffect(() => {
    if (!called.current) {
      effect();
      called.current = true;
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, ...deps);
}
