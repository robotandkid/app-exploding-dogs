/**
 * Difference a-b - elements that are in a but not in b
 *
 * @param a
 * @param b
 */
export function setDifference<T>(a: Set<T>, b: Set<T>) {
  return new Set([...a].filter((x) => !b.has(x)));
}

export function arrayDifference<T>(a: T[], b: T[]) {
  const setB = new Set(b);
  return new Set([...a].filter((x) => !setB.has(x)));
}
