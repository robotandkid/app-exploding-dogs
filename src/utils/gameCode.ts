const char = [
  ...Array.from({ length: 10 }).map((_, i) => i),
  ...Array.from({ length: 26 }).map((_, i) => String.fromCharCode(65 + i)),
] as const;

/**
 * returns six digit code XXXXXX where X is a number or upper case alpha
 */
export function gameCode() {
  return Array.from({ length: 6 })
    .map(() => {
      const index = Math.round(Math.random() * (char.length - 1));

      return char[index];
    })
    .join("");
}

export function formattedGameCode(possibleCode: string = "") {
  const _code = possibleCode.replace(/[^0-9a-zA-Z]/g, "");

  const formattedCode =
    _code.length > 3
      ? `${_code.substring(0, 3)} ${_code.substring(3, 6)}`
      : _code;

  return formattedCode.toUpperCase();
}

export function gameCodeQueryParam(formattedGameCode: string) {
  return formattedGameCode.replace(/ /g, "");
}
