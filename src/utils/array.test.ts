import { mod } from "./array";

describe("mod", () => {
  it("should be equivalent to % when a >= 0", () => {
    expect(mod(4, 9)).toEqual(4 % 9);
    expect(mod(9, 4)).toEqual(9 % 4);
    expect(mod(0, 6)).toEqual(0);
  });

  it("should always return non-negative values", () => {
    // because -1 = -1*2+1
    expect(mod(-1, 2)).toEqual(1);
    // because -8 = -2*5+2
    expect(mod(-8, 5)).toEqual(2);
    expect(mod(-282, 5)).toEqual(3);
  });
});
