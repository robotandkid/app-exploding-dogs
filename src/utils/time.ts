/**
 * current date's milliseconds - 1,000 ms * 60 s * 60 mins * 24 hrs * (# of days beyond one to go back)
 */
export const sixHoursAgo = () => new Date(Date.now() - 1000 * 60 * 60 * 6);
