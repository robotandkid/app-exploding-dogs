/**
 * The built-in modulo operation returns negative numbers,
 * which breaks math calculations.
 *
 * This is the standard modulo operation that always returns
 * non-negative numbers.
 * @param a any integer
 * @param n > 0
 */
export function mod(a: number, n: number) {
  // see https://en.wikipedia.org/wiki/Modulo_operation
  return a - Math.abs(n) * Math.floor(a / Math.abs(n));
}

/**
 * Treats an array like a circular array
 *
 * @param arr
 * @param base
 * @param i
 */
export function circularArray<T>(arr: T[], i: number) {
  return arr[mod(i, arr.length)];
}

export function toMapByKey<T>(arr: T[], key: keyof T) {
  return arr.reduce(
    (total, cur) => Object.assign(total, { [(cur as any)[key]]: cur }),
    {} as { [key: string]: T }
  );
}
