import { useRef } from "react";

/**
 * once the given value is true, it will forever return true
 * @param value
 */
export function useTakeUntilTrue(value: boolean | undefined) {
  const cache = useRef(false);

  if (!!value) {
    cache.current = true;
  }

  return cache.current;
}
